$(document).ready(function() {
  $("#form").validate({
    rules: {
      table_number: "required",
      status: "required"
    },
    messages: {
      table_number: "Please fill table number",
      status: "Please choose table availability"
    },
    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) {
      if (element.parent('.input-group').length) {
          error.insertAfter(element.parent());
      } else {
          error.insertAfter(element);
      }
    }
  });
});