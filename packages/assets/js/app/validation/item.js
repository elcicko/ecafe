$(document).ready(function() {
  $("#form").validate({
    rules: {
      name: "required",
      category: "required",
      price: "required",
      status: "required"
    },
    messages: {
      name: "Please fill item name",
      category: "Please choose category",
      price: "Please fill item price",
      status: "Please choose item status"
    },
    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) {
      if (element.parent('.input-group').length) {
          error.insertAfter(element.parent());
      } else {
          error.insertAfter(element);
      }
    }
  });
});