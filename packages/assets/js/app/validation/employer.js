$(document).ready(function() {
  $("#form").validate({
    rules: {
      employer_name: "required",
      employer_email: "required",
      level: "required",
      status: "required"
    },
    messages: {
      employer_name: "Please fill employer name",
      employer_email: "Please fill employer email",
      level: "Please choose employer level",
      status: "Please choose employer status"
    },
    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'help-block',
    errorPlacement: function(error, element) {
      if (element.parent('.input-group').length) {
          error.insertAfter(element.parent());
      } else {
          error.insertAfter(element);
      }
    }
  });
});