$(document).ready(function() {
  $("#form").validate({
      rules: {
          username: "required",
          nama: "required"
      },
      messages: {
          username: "Kolom Username Harus Diisi",
          nama: "Kolom Nama Harus Diisi"
      },
      highlight: function(element) {
          $(element).closest('.form-group').addClass('has-error');
      },
      unhighlight: function(element) {
          $(element).closest('.form-group').removeClass('has-error');
      },
      errorElement: 'span',
      errorClass: 'help-block',
      errorPlacement: function(error, element) {
        if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
      }
  });
});