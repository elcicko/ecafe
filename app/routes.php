<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');
Route::get('search', 'HomeController@search');

// AJAX
Route::post('validate', 'LoginController@validate');

// login
Route::get('/hubunganrelasi', 'HubunganRelasiController@index');
Route::get('/login', 'LoginController@index');
Route::get('logout', 'LoginController@logout');

// backend
Route::get('dashboard', 'BackendController@index');

// Table
Route::get('table', 'TableController@index');
Route::get('table/create', 'TableController@create');
Route::get('table/edit/{id}', 'TableController@edit');
Route::post('table/update/{id}', 'TableController@update');
Route::post('table/store', 'TableController@store');
Route::get('table/destroy/{id}', 'TableController@destroy');
Route::post('table/search', 'TableController@search');
Route::get('table/search', 'TableController@search');

// Category
Route::get('category', 'CategoryController@index');
Route::get('category/create', 'CategoryController@create');
Route::get('category/edit/{id}', 'CategoryController@edit');
Route::post('category/update/{id}', 'CategoryController@update');
Route::post('category/store', 'CategoryController@store');
Route::get('category/destroy/{id}', 'CategoryController@destroy');
Route::post('category/search', 'CategoryController@search');
Route::get('category/search', 'CategoryController@search');

// Item
Route::get('item', 'ItemController@index');
Route::get('item/create', 'ItemController@create');
Route::get('item/edit/{id}', 'ItemController@edit');
Route::post('item/update/{id}', 'ItemController@update');
Route::post('item/store', 'ItemController@store');
Route::get('item/destroy/{id}', 'ItemController@destroy');
Route::post('item/search', 'ItemController@search');
Route::get('item/search', 'ItemController@search');

// Order
Route::get('order', 'OrderController@index');
Route::post('order/search', 'OrderController@search');
Route::get('order/search', 'OrderController@search');

// Employer
Route::get('employer', 'EmployerController@index');
Route::get('employer/create', 'EmployerController@create');
Route::get('employer/edit/{id}', 'EmployerController@edit');
Route::post('employer/update/{id}', 'EmployerController@update');
Route::post('employer/store', 'EmployerController@store');
Route::get('employer/destroy/{id}', 'EmployerController@destroy');
Route::post('employer/search', 'EmployerController@search');
Route::get('employer/search', 'EmployerController@search');

// Menu
Route::get('menu', 'MenuController@index');
Route::get('menu/create', 'MenuController@create');
Route::post('menu/store', 'MenuController@store');
Route::get('menu/edit/{id}', 'MenuController@edit');
Route::get('menu/destroy/{id}', 'MenuController@destroy');
Route::post('menu/update/{id}', 'MenuController@update');
Route::post('menu/search', 'MenuController@search');
Route::get('menu/search', 'MenuController@search');

// Order
Route::get('order', 'OrderController@index');
Route::get('order/detail/{id}', 'OrderController@detail');
Route::post('order/search', 'OrderController@search');
Route::get('order/search', 'OrderController@search');

// Log
Route::get('logactivity', 'LogActivityController@index');
Route::post('logactivity/search', 'LogActivityController@search');
Route::get('logactivity/search', 'LogActivityController@search');

// Account
Route::get('account', 'AccountController@index');
Route::post('account/update', 'AccountController@updateaccount');

// Cafe
Route::get('cafe', 'CafeController@index');
Route::post('cafe/update/{id}', 'CafeController@update');

// Report
Route::get('config', 'ConfigController@index');
Route::post('config/update', 'ConfigController@update');
