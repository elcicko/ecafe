<ol class="breadcrumb">
  <li><a href="{{ url('dashboard') }}"><i class="mdi mdi-home"></i> Dashboard</a></li>
  <li><i class="mdi mdi-food"></i> {{ $title }}</li>
</ol>
<div class="row">
  <div class="col-lg-12">
    <div class="col-lg-12 well">
  		<form class="form-search" action="{{ url('item/search') }}" method="POST">
  			<div class="col-lg-3">
  				<div class="form-group">
  					<input type="text" class="form-control" placeholder="Item Name" name="q" id="q">
  				</div>
  			</div>
  			<div class="col-lg-3">
				 <div class="form-group">
		          {{ Form::select('category', $category, '', $attributes = array('class'=>'form-control', 'id'=>'category')) }}
		        </div>
			</div>
			<div class="col-lg-3">
				 <div class="form-group">
		          {{ Form::select('status', $status, '', $attributes = array('class'=>'form-control', 'id'=>'status')) }}
		        </div>
			</div>
  			<div class="col-lg-3">
  				<div class="form-group">
  					<button type="submit" class="btn btn-info" id="search"><i class="fa fa-search"></i> Search</button>
  					&nbsp; &nbsp;
  			  		<a class="btn btn-success" href="{{ url('item/create') }}"><i class="fa fa-plus"></i> Add New</a>
  				</div>
  			</div>
  		</form>
    </div>
    <div class="col-lg-12">
  		@if($count > 0)
  			@if(Session::has('message'))
			    <div class="alert alert-success">
			    	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <center>{{ Session::get('message') }}</center>
			    </div>
			@endif
			<?php 
	        $page = $items->getCurrentPage();
            if ($page == 1) {
            	$i = 1;
            } else {
            	$i = $items->getFrom();
            }
            ?>
            <div class="row">
           		@foreach($items as $item)
   					<div class="col-sm-6 col-md-2">
   			    		<div class="thumbnail">
   			    			{{ Item::get_pic($item->item_picture) }}
			      			<div class="caption">
      			        	<center><h3>{{ $item->item_name }}</h3>
                           	<center><p><b>{{ $item->status->item_status_name }}</b></p></center>
      			        		<p>
      			        		<a class="btn btn-default" data-toggle="modal" href="#detail{{ $i }}"><i class="mdi mdi-magnify"></i></a>
      			        		<a class="btn btn-default" href="{{ url('item/edit/'.$item->id.'') }}"><i class="mdi mdi-refresh"></i></a>
      			        		<a class="btn btn-default" data-toggle="modal" href="#confirm{{ $i }}"><i class="mdi mdi-delete"></i></a>
      			        		</p>
      			        		</center>
			      			</div>
   			    		</div>
   			  		</div>
   			  		<div class="modal fade" id="detail{{ $i }}" style="display:none;">
   	                	<div class="modal-dialog">
	                  		<div class="modal-content">
	                    		<div class="modal-header">
	                      	<button class="close" data-dismiss="modal">×</button>
	                      	<h4>Detail : {{ $item->item_name }}</h4>
	                    		</div>
			                    <div class="modal-body">
   			                    <p>Name     	: {{ $item->item_name }}
   			                    <p>Description  : {{ $item->item_description }}</p>
                                <p>Status : {{ $item->status->item_status_name }}<p>
			                    </div>
			                    <div class="modal-footer"></div>
	                  		</div>
   	                	</div>
   		            </div>

   			  		<div class="modal fade" id="confirm{{ $i }}" style="display:none;">
	                	<div class="modal-dialog">
                  		<div class="modal-content">
                    		<div class="modal-header">
                      			<button class="close" data-dismiss="modal">×</button>
                      			<h4>Confirm</h4>
                    		</div>
		                    <div class="modal-body">
		                      <p>Are You Sure To Remove This Item ? This Process Cannot Be Undone</p>
		                    </div>
		                    <div class="modal-footer">
		                      <a class="btn btn-success" href="{{ url('item/destroy/'.$item->id.''); }}" >Yes</a>
		                      <a href="#" data-dismiss="modal" class="btn btn-danger">Cancel</a>
		                    </div>
                  		</div>
	                	</div>
   		         </div>
   		         <?php $i++; ?>
   			  	@endforeach
   			</div>
    		{{-- <table class="tablesorter" width="100%" cellpadding="2" cellspacing="2">
				<thead>
					<th width="4%"><center>#</center></th>
					<th width="15%"><center>Item Name</center></th>
					<th width="8%"><center>Category</center></th>
					<th width="7%"><center>Price</center></th>
					<th width="7%"><center>Discount</center></th>
					<th width="20%"><center>Picture</center></th>
					<th width="7%"><center>Status</center></th>
					<th width="5%"><center>Action</center></th>
				</thead>
				<tbody>
					@foreach($items as $item) 
						<tr>
							<td><center>{{ ++$i }}</center></td>
							<td>{{ $item->item_name }}</td>
							<td><center>{{ $item->category->category_name }}</center></td>
							<td align="right">
								{{ Money::rupiah($item->item_price) }}
							</td>
							<td align="right">
								{{ Money::rupiah($item->item_price) }}
							</td>
							<td><center>{{ $item->item_picture }}</center></td>
							<td>
								@if ($item->item_status_id == 1)
									<center><span class="label label-success">{{ $item->status->item_status_name }}</span></center>
								@elseif ($item->item_status_id == 2)
									<center><span class="label label-danger">{{ $item->status->item_status_name }}</span></center>
								@endif
							</td>
							<td>
								<center>
									<a href="{{ url('item/edit/'.$item->id.'') }}"><i class="mdi mdi-refresh"></i></a>
									&nbsp;
									<a data-toggle="modal" href="#confirm{{ $i }}"><i class="mdi mdi-delete"></i></a>
								</center>
			                </td>
						</tr>
						<div class="modal fade" id="confirm{{ $i }}" style="display:none;">
		                	<div class="modal-dialog">
		                  		<div class="modal-content">
		                    		<div class="modal-header">
		                      			<button class="close" data-dismiss="modal">×</button>
		                      			<h4>Confirm</h4>
		                    		</div>
				                    <div class="modal-body">
				                      <p>Are You Sure To Remove This Item ?</p>
				                    </div>
				                    <div class="modal-footer">
				                      <a class="btn btn-primary" href="{{ url('item/destroy/'.$item->id.''); }}" >Yes</a>
				                      <a href="#" data-dismiss="modal" class="btn btn-danger">No</a>
				                    </div>
		                  		</div>
		                	</div>
		              	</div>
					@endforeach
				</tbody>
			</table> --}}
			<ul class="pagination">
			{{ $items->links() }}
			</ul>
		@else
		    <div class="alert alert-danger"><center>NO ITEM AVAILABLE</center></div>
	    @endif
	</div>
  </div>
</div>
