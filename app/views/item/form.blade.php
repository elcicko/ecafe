<ol class="breadcrumb">
  <li><a href="{{ url('dashboard') }}"><i class="mdi mdi-home"></i> Dashboard</a></li>
  <li><a href="{{ url('item') }}"><i class="mdi mdi-food"></i> {{ $title }}</a></li>
  <li>{{ $action_title }}</li>
</ol>
<script src="{{ asset('packages/assets/js/app/validation/item.js') }}"></script>
<script src="{{ asset('packages/assets/js/plugins/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('packages/assets/js/plugins/ckeditor/adapters/jquery.js') }}"></script>
<script type="text/javascript">
$(document).ready( function() {
  CKEDITOR.replace( 'description', {
    height: '100%'  
  });
});
</script>
<div class="panel panel-info">
  <div class="panel-heading"><i class="mdi mdi-food"></i> <?php echo $action_title; ?></div>
    <div class="panel-body">
    <form class="form-vertical" id="form" method="POST" action="{{ $form_action }}" enctype="multipart/form-data">
      <div class="col-lg-6">
        <div class="form-group">
        <label for="order">Item Name</label>  
        <input id="name" name="name" value="{{ isset($item->item_name) ? $item->item_name : '' }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Category</label>  
          {{ Form::select('category', $category, isset($item->category_id) ? $item->category_id : '', $attributes = array('class'=>'form-control', 'id'=>'category')) }}
        </div>

        <div class="form-group">
          <label for="order">Price</label>  
          <input id="price" name="price" value="{{ isset($item->price) ? $item->price : '' }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Discount</label>  
          <input id="discount" name="discount" value="{{ isset($item->discount) ? $item->discount : '' }}" type="text" placeholder="" class="form-control input-md">
        </div>

        <div class="form-group">
          <label for="order">Status</label>  
          {{ Form::select('status', $status, isset($item->item_status_id) ? $item->item_status_id : '', $attributes = array('class'=>'form-control', 'id'=>'status')) }}
        </div>

      <div class="form-group">
        <label for="order">Description</label>
        <textarea id="description" name="description" class="ckeditor form-control" >{{ isset($item->item_description) ? $item->item_description : '' }}</textarea>
      </div>

      <div class="form-group">
          <label for="order">Picture</label>  
          <input id="picture" name="picture" type="file" placeholder="">
        </div>

      <div class="form-group">
        <br>
        <input id="old_picture" name="old_picture" type="hidden" value="{{ isset($item->item_picture) ? $item->item_picture : '' }}">
        <button id="" name="" class="btn btn-success"><i class="mdi mdi-content-save"></i> Save Item</button>
      </div>
      </div>
      <div class="col-lg-6">
       <h5><i class="fa fa-question-circle"></i> Quick Tips</h5>
        <p>
        Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren
        Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren.
        </p>
         <p>
        Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren
        Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren.
        </p>
      </div>
    </form>
  </div>
</div>