<ol class="breadcrumb">
  <li><a href="{{ url('dashboard') }}"><i class="mdi mdi-home"></i> Dashboard</a></li>
  <li><i class="mdi mdi-folder"></i> {{ $title }}</li>
</ol>
<div class="row">
  <div class="col-lg-12">
    <div class="col-lg-12 well">
      <br>
  		<form class="form-search" action="{{ $form_action }}" method="POST">
  			<div class="col-lg-5">
  				<div class="form-group">
  					<input type="text" class="form-control" placeholder="Category Name" name="q" id="q">
  				</div>
  			</div>
  			<div class="col-lg-4">
  				<div class="form-group">
  					<button type="submit" class="btn btn-info" id="search"><i class="mdi mdi-magnify"></i> Search</button>
  					&nbsp; &nbsp;
  			  		<a class="btn btn-success" href="{{ url('category/create') }}"><i class="mdi mdi-plus"></i> Add New</a>
  				</div>
  			</div>
  		</form>
    </div>
    <div class="col-lg-12">
  		@if($count > 0)
  			@if(Session::has('message'))
			    <div class="alert alert-success">
			    	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <center>{{ Session::get('message') }}</center>
			    </div>
			@endif
			<?php
	        $page = $categories->getCurrentPage();
            if ($page == 1) {
            	$i = 0;
            } else {
            	$i = $categories->getFrom();
            }
         ?>
         <div class="row">
            @foreach($categories as $category)
               <div class="col-sm-6 col-md-2">
                  <div class="thumbnail">
                     <img src="{{ url($category->category_picture) }}" alt="{{ $category->category_name }}">
                     <div class="caption">
                        <center><h3>{{ $category->category_name }}</h3>
                        <p>
                          <a class="btn btn-default" href="{{ url('category/edit/'.$category->id.'') }}"><i class="mdi mdi-refresh"></i></a>
                          <a class="btn btn-default" data-toggle="modal" href="#confirm{{ $i }}"><i class="mdi mdi-delete"></i></a>
                        </p>
                        </center>
                     </div>
                  </div>
               </div>

               <div class="modal fade" id="confirm{{ $i }}" style="display:none;">
                  <div class="modal-dialog">
                     <div class="modal-content">
                     <div class="modal-header">
                           <button class="close" data-dismiss="modal">×</button>
                           <h4>Confirm</h4>
                     </div>
                       <div class="modal-body">
                         <p>Are You Sure To Remove This Category ? This Process Cannot Be Undone</p>
                       </div>
                       <div class="modal-footer">
                         <a class="btn btn-success" href="{{ url('category/destroy/'.$category->id.''); }}" >Yes</a>
                         <a href="#" data-dismiss="modal" class="btn btn-danger">Cancel</a>
                       </div>
                     </div>
                  </div>
               </div>
               <?php $i++; ?>
            @endforeach
         </div>
			<ul class="pagination">
			{{ $categories->links() }}
			</ul>
		@else
		    <div class="alert alert-danger"><center>NO CATEGORY AVAILABLE</center></div>
	    @endif
	</div>
  </div>
</div>
