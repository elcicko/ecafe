<ol class="breadcrumb">
  <li><a href="{{ url('dashboard') }}"><i class="mdi mdi-home"></i> Dashboard</a></li>
  <li><i class="mdi mdi-folder"></i> {{ $title }}</li>
  <li>{{ $action_title }}</li>
</ol>
<script src="{{ asset('packages/assets/js/app/validation/category.js') }}"></script>
<div class="panel panel-info">
  <div class="panel-heading"><i class="mdi mdi-folder"></i> <?php echo $action_title; ?></div>
  <div class="panel-body">
    <form class="form-vertical" id="form" method="POST" action="{{ $form_action }}" enctype="multipart/form-data">
      <fieldset>
        <div class="col-lg-6">
          <div class="form-group">
            <label for="order">Category Name</label>
            <input id="category_name" name="category_name" value="{{ isset($category->category_name) ? $category->category_name : '' }}" type="text" placeholder="" class="form-control input-md">
          </div>
          <div class="form-group">
            <label for="order">Picture</label>
            <input id="category_picture" name="category_picture" type="file">
          </div>
          <div class="form-group">
            <br>
            <input type="hidden" name="old_category_picture" id="old_category_picture" value="{{ isset($category->category_picture) ? $category->category_picture : '' }}">
            <button class="btn btn-success"><i class="mdi mdi-content-save"></i> Save Category</button>
          </div>
        </div>
        <div class="col-lg-6">
          <h5><i class="fa fa-question-circle"></i> Quick Tips</h5>
        <p>
        Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren
        Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren.
        </p>
         <p>
        Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren
        Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren.
        </p>
        </div>
      </fieldset>
    </form>
  </div>
</div>