<ol class="breadcrumb">
  <li><a href="{{ url('dashboard') }}"><i class="mdi mdi-home"></i> Dashboard</a></li>
  <li><i class="mdi mdi-chair-school"></i> {{ $title }}</li>
</ol>
<div class="row">
  <div class="col-lg-12">
    <div class="col-lg-12 well">
      <br>
  		<form class="form-search" action="{{ $form_action }}" method="POST">
         <div class="col-lg-4">
  				<div class="form-group">
                {{ Form::select('status', $status, '', $attributes = array('class'=>'form-control', 'id'=>'status')) }}
        		</div>
  			</div>
  			<div class="col-lg-4">
  				<div class="form-group">
  					<button type="submit" class="btn btn-info" id="search"><i class="mdi mdi-magnify"></i> Search</button>
  					&nbsp; &nbsp;
  					<a class="btn btn-success" href="{{ url('table/create') }}"><i class="mdi mdi-plus"></i> Add New</a>
  				</div>
  			</div>
  		</form>
    </div>
    <div class="col-lg-12">
  		@if($count > 0)
  			@if(Session::has('message'))
			    <div class="alert alert-success">
			    	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <center>{{ Session::get('message') }}</center>
			    </div>
			@endif
			<?php
	        $page = $tables->getCurrentPage();
            if ($page == 1) {
            	$i = 1;
            } else {
            	$i = $tables->getFrom();
            }
        	?>
        	<div class="row">
        		@foreach($tables as $table)
					<div class="col-sm-6 col-md-2">
			    		<div class="thumbnail">
                <a class="btn btn-default" data-toggle="modal" href="#detail{{ $i }}">
			    			@if ($table->status == 'Available')
                    <img src="{{ url('packages/img/icon/table-available.png') }}" alt="{{ $table->table_number }}">
			      		@else
			      		   <img src="{{ url('packages/img/icon/table-unavailable.png') }}" alt="{{ $table->table_number }}">
			      		@endif
                </a>
			      		<div class="caption">
			        		<center><h3>{{ $table->table_number }}</h3>
			        		<p>
   			        		<a class="btn btn-default" href="{{ url('table/edit/'.$table->id.'') }}"><i class="mdi mdi-refresh"></i></a>
   			        		<a class="btn btn-default" data-toggle="modal" href="#confirm{{ $i }}"><i class="mdi mdi-delete"></i></a>
			        		</p>
			        		</center>
			      			</div>
			    		</div>
			  		</div>
			  		<div class="modal fade" id="detail{{ $i }}" style="display:none;">
                	<div class="modal-dialog">
               		<div class="modal-content">
                    		<div class="modal-header">
                   			<button class="close" data-dismiss="modal">×</button>
                   			<h4>Detail : {{ $table->table_number }}</h4>
                    		</div>
   	                  <div class="modal-body">
   	                       <p>Description : {{ $table->table_desc }}</p>
                             <p> Status : <b>{{ $table->status }}</b></p>
                        </div>
   	                  <div class="modal-footer"></div>
               		</div>
                	</div>
		         </div>

			  		<div class="modal fade" id="confirm{{ $i }}" style="display:none;">
                	<div class="modal-dialog">
               		<div class="modal-content">
                 		<div class="modal-header">
                   			<button class="close" data-dismiss="modal">×</button>
                   			<h4>Confirm</h4>
                 		</div>
	                    <div class="modal-body">
	                      <p>Are You Sure To Remove This Table ? This Process Cannot Be Undone</p>
	                    </div>
	                    <div class="modal-footer">
	                      <a class="btn btn-success" href="{{ url('table/destroy/'.$table->id.''); }}" >Yes</a>
	                      <a href="#" data-dismiss="modal" class="btn btn-danger">Cancel</a>
	                    </div>
               		</div>
                	</div>
		         </div>
		         <?php $i++; ?>
			  	@endforeach
			</div>
			<ul class="pagination">
			{{ $tables->links() }}
			</ul>
		@else
		    <div class="alert alert-danger"><center>NO TABLE AVAILABLE</center></div>
	    @endif
	</div>
  </div>
</div>
