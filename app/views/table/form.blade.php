<ol class="breadcrumb">
  <li><a href="{{ url('dashboard') }}"><i class="fa fa-home"></i> Dashboard</a></li>
  <li><a href="{{ url('table') }}"><i class="mdi mdi-chair-school"></i> {{ $title }}</a></li>
  <li>{{ $action_title }}</li>
</ol>
<script src="{{ asset('packages/assets/js/app/validation/table.js') }}"></script>
<script src="{{ asset('packages/assets/js/plugins/ckeditor/ckeditor.js') }}"></script>
<div class="panel panel-info">
  <div class="panel-heading"><i class="mdi mdi-chair-school"></i> <?php echo $action_title; ?></div>
  <div class="panel-body">
    <form class="form-vertical" id="form" method="POST" action="{{ $form_action }}">
      <fieldset>
        <div class="col-lg-6">
          <div class="form-group">
            <label for="order">Table Name or Number</label>
            <input id="table_number" name="table_number" value="{{ isset($table->table_number) ? $table->table_number : '' }}" type="text" placeholder="" class="form-control input-md">
         </div>
          <div class="form-group">
            <label for="order">Table Description</label>
            <textarea id="table_desc" name="table_desc" class="ckeditor">{{ isset($table->table_desc) ? $table->table_desc : '' }}</textarea>
          </div>
          <div class="form-group">
            <label for="order">Availability</label>
            {{ Form::select('status', $status, isset($table->status) ? $table->status : '', $attributes = array('class'=>'form-control', 'id'=>'status')) }}
          </div>
          <div class="form-group">
            <button id="" name="" class="btn btn-success"><i class="mdi mdi-content-save"></i> Save Table</button>
          </div>
        </div>
        <div class="col-lg-6">
        <h5><i class="fa fa-question-circle"></i> Quick Tips</h5>
        <p>
        Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren
        Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren.
        </p>
         <p>
        Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren
        Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren Lorem Ipsum Doren.
        </p>
        </div>
      </fieldset>
    </form>
  </div>
</div>