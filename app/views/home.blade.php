<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Wikafe">
  <title>Wikafe</title>
  <link rel="shortcut icon" href="{{ url('packages/front/images/gt_favicon.png') }}">
  <link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
  <link rel="stylesheet" href="{{ url('packages/front/css/bootstrap.min.css') }}">
  <link href="{{ url('packages/bootflat/css/bootflat.css') }}" rel="stylesheet">
  <link href="{{ url('packages/assets/mdi/css/materialdesignicons.min.css') }}" rel="stylesheet" />
  <link rel="stylesheet" href="{{ url('packages/front/css/font-awesome.min.css') }}">
  <link rel="stylesheet" href="{{ url('packages/front/css/bootstrap-theme.css') }}" media="screen" >
  <link rel="stylesheet" href="{{ url('packages/front/css/main.css') }}">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="assets/js/html5shiv.js"></script>
  <script src="assets/js/respond.min.js"></script>
  <![endif]-->
</head>

<body>
  <!-- Fixed navbar -->
  <div class="navbar navbar-inverse navbar-fixed-top headroom" >
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        <a class="navbar-brand" href="#"><img src="{{ url('packages/img/logo.png') }}" alt="Wikafe" width="80%"></a>
      </div>
      <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav pull-right">
          <li><a class="btn btn-info" data-toggle="modal" href="#login">MASUK</a></li>
          <li><a class="btn btn-info" href="#">DAFTAR</a></li>
        </ul>
      </div>
    </div>
  </div> 
  <div class="modal fade" id="login" style="display:none;">
    <div class="modal-dialog" style="width:380px;">
      <div class="modal-content" >
        <div class="modal-header">
              <button class="close" data-dismiss="modal">×</button>
          </div>
          <div class="modal-body">
          <center>
            <br>
            <img src="{{ url('packages/img/logo.png') }}" alt="Wikafe">
            <br><br><br>
            <form class="form-vertical">
              <input type="text" class="form-control input-lg" name="username" id="username" placeholder="Username">
              <br>
              <input type="password" class="form-control input-lg" name="password" id="password" placeholder="Password">
              <br>
              <button class="btn btn-info btn-lg"><i class="fa fa-unlock"></i> Login</button>
             <br><br>
              <a href="#">Lupa Password ?</a>
            </form>
          </center>
          </div>
          <div class="modal-footer"></div>
        </div>
      </div>
  </div>
  <header id="head" class="secondary"></header>

  <!-- container -->
  <div class="container">
    <div class="row">
      <div class="col-md-8 maincontent">
      <br><br>
        <h4>Bingung ? Silahkan ketik sesuatu dibawah ini :)</h4>
        <form>
          <input type="text" class="form-control input-md" name="q" id="q" placeholder="Cari kota, restoran, kafe atau makanan">
          <br>
          <button class="btn btn-info"><i class="fa fa-search"></i> Cari</button>
        </form>
      </div>

      <aside class="col-md-4 sidebar sidebar-right">
        <div class="row widget">
          <div class="col-xs-12">
            <h4>Lorem ipsum dolor sit</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere, ratione delectus reiciendis nulla nisi pariatur molestias animi eos repellat? Vel.</p>
          </div>
        </div>
        <div class="row widget">
          <div class="col-xs-12">
            <h4>Lorem ipsum dolor sit</h4>
            <p><img src="{{ url('packages/front/images/1.jpg') }}" alt=""></p>
          </div>
        </div>
        <div class="row widget">
          <div class="col-xs-12">
            <h4>Lorem ipsum dolor sit</h4>
            <p><img src="{{ url('packages/front/images/2.jpg') }}" alt=""></p>
            <p>Qui, debitis, ad, neque reprehenderit laborum soluta dolor voluptate eligendi enim consequuntur eveniet recusandae rerum? Atque eos corporis provident tenetur.</p>
          </div>
        </div>
      </aside>
    </div>
  </div>
  <footer id="footer" class="top-space">
    <div class="footer1">
      <div class="container">
        <div class="row">
          <div class="col-md-3 widget">
            <h3 class="widget-title">Contact</h3>
            <div class="widget-body">
              <p>+234 23 9873237<br>
                <a href="mailto:#">some.email@somewhere.com</a><br>
                <br>
                234 Hidden Pond Road, Ashland City, TN 37015
              </p>  
            </div>
          </div>

          <div class="col-md-3 widget">
            <h3 class="widget-title">Contact</h3>
            <div class="widget-body">
              <p>+234 23 9873237<br>
                <a href="mailto:#">some.email@somewhere.com</a><br>
                <br>
                234 Hidden Pond Road, Ashland City, TN 37015
              </p>  
            </div>
          </div>

          <div class="col-md-3 widget">
            <h3 class="widget-title">Follow me</h3>
            <div class="widget-body">
              <p class="follow-me-icons clearfix">
                <a href=""><i class="fa fa-twitter fa-2"></i></a>
                <a href=""><i class="fa fa-dribbble fa-2"></i></a>
                <a href=""><i class="fa fa-github fa-2"></i></a>
                <a href=""><i class="fa fa-facebook fa-2"></i></a>
              </p>  
            </div>
          </div>

          <div class="col-md-3 widget">
            <h3 class="widget-title">Tentang Kami</h3>
            <div class="widget-body">
              <p>Tujuan kami adalah memberikan kemudahan bagi Anda untuk mencari tempat yang tepat untuk hangout, makan atau sekedar menikmati secangkir kopi.</p>
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="footer2">
      <div class="container">
        <div class="row">
          
          <div class="col-md-6 widget">
            <div class="widget-body">
              <p class="simplenav">
                <a href="#">Home</a> | 
                <a href="about.html">Tentang Kami</a> |
                <a href="sidebar-right.html">FAQ</a> |
                <a href="contact.html">Kebijakan Privasi</a>
              </p>
            </div>
          </div>

          <div class="col-md-6 widget">
            <div class="widget-body">
              <p class="text-right">
                Copyright &copy; {{ date('Y')}}, Made with <i class="fa fa-coffee"></i> by <a href="#">Wikafe</a> 
              </p>
            </div>
          </div>

        </div> <!-- /row of widgets -->
      </div>
    </div>
  </footer> 
  <!-- JavaScript libs are placed at the end of the document so the pages load faster -->
  <script src="{{ url('packages/assets/js/jquery/jquery.min.js') }}"></script>
  <script src="{{ url('packages/assets/js/bootstrap/bootstrap.min.js') }}"></script>
  <script src="{{ url('packages/front/js/headroom.min.js') }}"></script>
  <script src="{{ url('packages/front/js/jQuery.headroom.min.js') }}"></script>
  <script src="{{ url('packages/front/js/template.js') }}"></script>
</body>
</html>