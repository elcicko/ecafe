<ol class="breadcrumb">
  <li><a href="{{ url('dashboard') }}"><i class="mdi mdi-home"></i> Dashboard</a></li>
  <li><a href="{{ url('employer') }}"><i class="mdi mdi-account"></i> {{ $title }}</a></li>
  <li>{{ $action_title }}</li>
</ol>
<script src="{{ asset('packages/assets/js/app/validation/employer.js') }}"></script>
<div class="panel panel-info">
  <div class="panel-heading"><i class="mdi mdi-account"></i></i> <?php echo $action_title; ?></div>
  <div class="panel-body">
    <form class="form-vertical" id="form" method="POST" action="{{ $form_action }}" enctype="multipart/form-data">
      <fieldset>
        @if(Session::has('message'))
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <center>{{ Session::get('message') }}</center>
          </div>
        @endif
        <div class="col-lg-6">
          <div class="form-group">
            <label for="order">Employer's Name</label>
            <input id="employer_name" name="employer_name" value="{{ isset($employer->employer_name) ? $employer->employer_name : '' }}" type="text" placeholder="" class="form-control input-md">
          </div>
          <div class="form-group">
            <label for="order">Email</label>
            <input id="employer_email" name="employer_email" value="{{ isset($employer->employer_email) ? $employer->employer_email : '' }}" type="text" placeholder="" class="form-control input-md">
          </div>
        </div>

        <div class="col-lg-6">
          <div class="form-group">
            <label for="order">Level / Position</label>
            {{ Form::select('level', $level, isset($employer->level_id) ? $employer->level_id : '', $attributes = array('class'=>'form-control', 'id'=>'level')) }}
          </div>
          <div class="form-group">
             <label for="order">Status</label>
             {{ Form::select('status', $status, isset($employer->employer_activated) ? $employer->employer_activated : '', $attributes = array('class'=>'form-control', 'id'=>'status')) }}
          </div>
        </div>

        <div class="col-lg-6">
          <div class="form-group">
            <br><button class="btn btn-success" type="submit"><i class="mdi mdi-content-save"></i> Save Employer</button>
          </div>
          <i><span style="color:red;">*</span> Every employer will have a default password : <b>employer</b></i>
        </div>
      </fieldset>
    </form>
  </div>
</div>
