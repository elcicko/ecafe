<ol class="breadcrumb">
  <li><a href="{{ url('dashboard') }}"><i class="mdi mdi-home"></i> Dashboard</a></li>
  <li><i class="mdi mdi-account"></i> {{ $title }}</li>
</ol>
<div class="row">
  <div class="col-lg-12">
  		<div class="col-lg-12 well">
  			<br>
	  		<form class="form-search" action="{{ url('employer/search') }}" method="POST">
	  			<div class="col-lg-3">
	  				<div class="form-group">
	  					<input type="text" class="form-control" placeholder="Employer's Name" name="q" id="q">
	  				</div>
	  			</div>
				<div class="col-lg-3">
					 <div class="form-group">
			          {{ Form::select('level', $level, '', $attributes = array('class'=>'form-control', 'id'=>'level')) }}
			        </div>
				</div>
            	<div class="col-lg-3">
					<div class="form-group">
						{{ Form::select('status', $status, '', $attributes = array('class'=>'form-control', 'id'=>'status')) }}
					</div>
				</div>
				<div class="col-lg-3">
					<div class="form-group">
						<button type="submit" class="btn btn-info" id="search"><i class="mdi mdi-magnify"></i> Search</button>
						&nbsp; &nbsp;
						<a class="btn btn-success" href="{{ url('employer/create') }}"><i class="mdi mdi-plus"></i> Add New</a>
					</div>
				</div>
			</form>
		</div>
		<div class="col-lg-12 well">
	  		@if($count > 0)
	  			@if(Session::has('message'))
				    <div class="alert alert-success">
				    	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <center>{{ Session::get('message') }}</center>
				    </div>
				@endif
            <?php
   	        $page = $employers->getCurrentPage();
               if ($page == 1) {
               	$i = 1;
               } else {
               	$i = $employers->getFrom();
               }
           	?>
            <div class="row">
           		@foreach($employers as $employer)
   					<div class="col-sm-6 col-md-2">
   			    		<div class="thumbnail">
   			    			<a class="btn btn-default" data-toggle="modal" href="#detail{{ $i }}">
   			    			{{ Employer::get_pic($employer->employer_activated, $employer->level_id) }}
   			    			</a>
			      			<div class="caption">
      			        		<center><h4>{{ $employer->employer_name }}</h4>
                           		<center><p>{{ $employer->level->level_name }}</p></center>
      			        		<p>
      			        		<a class="btn btn-default" href="{{ url('employer/edit/'.$employer->id.'') }}"><i class="mdi mdi-refresh"></i></a>
      			        		<a class="btn btn-default" data-toggle="modal" href="#confirm{{ $i }}"><i class="mdi mdi-delete"></i></a>
      			        		</p>
      			        		</center>
			      			</div>
   			    		</div>
   			  		</div>
   			  		<div class="modal fade" id="detail{{ $i }}" style="display:none;">
   	                	<div class="modal-dialog">
	                  		<div class="modal-content">
	                    		<div class="modal-header">
	                      			<button class="close" data-dismiss="modal">×</button>
	                      			<h4>Detail : {{ $employer->employer_name }}</h4>
	                    		</div>
			                    <div class="modal-body">
	   			                    <p>Name     : {{ $employer->employer_name }}
	   			                    <p>Email    : {{ $employer->employer_email }}</p>
	                                <p>Position : {{ $employer->level->level_name }}<p>
	                                <p>Active   : <b>{{ ucwords($employer->employer_activated) }}</b><p>
			                    </div>
			                    <div class="modal-footer"></div>
	                  		</div>
   	                	</div>
   		            </div>

   			  		<div class="modal fade" id="confirm{{ $i }}" style="display:none;">
	                	<div class="modal-dialog">
                  		<div class="modal-content">
                    		<div class="modal-header">
                      			<button class="close" data-dismiss="modal">×</button>
                      			<h4>Confirm</h4>
                    		</div>
		                    <div class="modal-body">
		                      <p>Are You Sure To Remove This Employer ? This Process Cannot Be Undone</p>
		                    </div>
		                    <div class="modal-footer">
		                      <a class="btn btn-success" href="{{ url('employer/destroy/'.$employer->id.''); }}" >Yes</a>
		                      <a href="#" data-dismiss="modal" class="btn btn-danger">Cancel</a>
		                    </div>
                  		</div>
	                	</div>
   		         </div>
   		         <?php $i++; ?>
   			  	@endforeach
   			</div>
   			<ul class="pagination">
   			{{ $employers->links() }}
         	</ul>
			@else
		    <div class="alert alert-danger"><center>NO EMPLOYER IS FOUNDED</center></div>
		   @endif
		</div>
  	</div>
</div>
