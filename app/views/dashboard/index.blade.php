<ol class="breadcrumb">
  <li><a href="#"><i class="mdi mdi-home"></i> Dashboard</a></li>
</ol>
<div class="row">
    <div class="col-lg-12">
		<div class="panel panel-info">
    		<div class="panel-heading"><i class="mdi mdi-silverware"></i> LATEST ORDER</div>
      			<div class="panel-body">
					@if ($count > 0)
					<?php $i = 0; ?>
	        		<table class="tablesorter" width="100%" cellpadding="3" cellspacing="3">
						<thead>
							<th width="5%"><center>#</center></th>
							<th width="10%"><center>Invoice</center></th>
							<th width="10%"><center>Date</center></th>
							<th width="10%"><center>Table</center></th>
							<th width="10%"><center>Status</center></th>
						</thead>
						<tbody>
							@foreach($order as $val) 
								<tr>
									<td><center>{{ ++$i }}</center></td>
									<td>{{ $val->order_invoice }}</td>
									<td>{{ $val->created_at }}</td>
									<td><center>{{ $val->table->table_name }}</center></td>
									<td><center>{{ $val->table->table_name }}</center></td>
								</tr>
							@endforeach
							<tr>
								<td colspan="8">
								<center>
									<a href="{{ url('order') }}"><i class="mdi mdi-arrow-right-bold"></i> SEE ALL ORDERS</a>
								</center>
								</td>
							</tr>
						</tbody>
					</table>
	        	@else
	        		<div class="alert alert-danger"><center>NO ORDER AVAILABLE</center></div>
	        	@endif
      		</div>
    	</div>
	</div>
</div>

<div class="row">
    <div class="col-lg-6">
		<div class="panel panel-info">
    		<div class="panel-heading"><i class="mdi mdi-chart-areaspline"></i> STATISTIC</div>
      			<div class="panel-body">
					@if ($count > 0)
					<?php $i = 0; ?>
	        		<table class="tablesorter" width="100%" cellpadding="3" cellspacing="3">
						<thead>
							<th width="5%"><center>#</center></th>
							<th width="10%"><center>Invoice</center></th>
							<th width="10%"><center>Date</center></th>
							<th width="10%"><center>Table</center></th>
							<th width="10%"><center>Status</center></th>
						</thead>
						<tbody>
							@foreach($order as $val) 
								<tr>
									<td><center>{{ ++$i }}</center></td>
									<td>{{ $val->order_invoice }}</td>
									<td>{{ $val->created_at }}</td>
									<td><center>{{ $val->table->table_name }}</center></td>
									<td><center>{{ $val->table->table_name }}</center></td>
								</tr>
							@endforeach
							<tr>
								<td colspan="8">
								<center>
									<a href="{{ url('order') }}"><i class="mdi mdi-arrow-right-bold"></i> SEE ALL ORDERS</a>
								</center>
								</td>
							</tr>
						</tbody>
					</table>
	        	@else
	        		<div class="alert alert-danger"><center>NO DATA AVAILABLE</center></div>
	        	@endif
      		</div>
    	</div>
	</div>
	<div class="col-lg-6">
		<div class="panel panel-info">
    		<div class="panel-heading"><i class="mdi mdi-comment"></i> RATINGS & TESTIMONIALS</div>
      			<div class="panel-body">
					@if ($count > 0)
					<?php $i = 0; ?>
	        		<table class="tablesorter" width="100%" cellpadding="3" cellspacing="3">
						<thead>
							<th width="5%"><center>#</center></th>
							<th width="10%"><center>Invoice</center></th>
							<th width="10%"><center>Date</center></th>
							<th width="10%"><center>Table</center></th>
							<th width="10%"><center>Status</center></th>
						</thead>
						<tbody>
							@foreach($order as $val) 
								<tr>
									<td><center>{{ ++$i }}</center></td>
									<td>{{ $val->order_invoice }}</td>
									<td>{{ $val->created_at }}</td>
									<td><center>{{ $val->table->table_name }}</center></td>
									<td><center>{{ $val->table->table_name }}</center></td>
								</tr>
							@endforeach
							<tr>
								<td colspan="8">
								<center>
									<a href="{{ url('order') }}"><i class="mdi mdi-arrow-right-bold"></i> SEE ALL ORDERS</a>
								</center>
								</td>
							</tr>
						</tbody>
					</table>
	        	@else
	        		<div class="alert alert-danger"><center>NO DATA AVAILABLE</center></div>
	        	@endif
      		</div>
    	</div>
	</div>
</div>