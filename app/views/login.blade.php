<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Wikafe">
    <title>Wikafe</title>
    <link href="{{ asset('packages/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ url('packages/bootflat/css/bootflat.css') }}" rel="stylesheet">
    <link href="{{ url('packages/assets/mdi/css/materialdesignicons.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('packages/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/css/style-responsive.css') }}" rel="stylesheet">
    </head>
  	<body>
		<div id="login-page">
		  	<div class="container">
		  		{{ Form::open(array('url'=>$form_action, 'class'=>'form-login', 'id'=>'form-login')) }}
		  		<br><br>
	  			<center><img src="{{ asset('packages/img/login_logo.png') }}" width="30%"></center>
	  			<br>
	  			<div class="login-wrap">
					{{ Form::text('username', null, array('class'=>'form-control input-lg', 'placeholder'=>'Username','autofocus'=>'autofocus', 'required' => 'required')) }}
					<br>
					{{ Form::password('password', array('class'=>'form-control input-lg', 'placeholder'=>'Password','required' => 'required')) }}
					<br>
					<button id="login_button" class="btn btn-theme btn-block" type="submit"><i class="mdi mdi-lock"></i> LOG IN</button>
					<br>
					@if(Session::has('error'))
						<div class="alert alert-danger">
							<center>{{ Session::get('error') }}</center>
						</div>
					@endif
					{{ Form::close() }}
				</div>
			    <br>
		  		<center>
		  			<p>Copyright &copy; {{ date('Y') }} Wikafe
		  		</center>
		  	</div>
	  	</div>
  	</body>
</html>
