<!DOCTYPE HTML>
<html>
    <head>
        <title>Open Cafe</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
        <link rel="stylesheet" href="{{ url('packages/assets/css/bootstrap/bootstrap.css') }}" />
        <link href="{{ url('packages/bootflat/css/bootflat.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ url('packages/front/assets/css/main.css') }}" />
        <link href="{{ url('packages/assets/fonts/mdi/css/materialdesignicons.min.css') }}" rel="stylesheet" />
        <!--[if lte IE 8]><link rel="stylesheet" href="assets/css/ie8.css" /><![endif]-->
    </head>
    <body class="landing">
        <div id="page-wrapper">
            <!-- Header -->
                <header id="header" class="alt">
                    <h1><img src="{{ url('packages/img/logo.png') }}" width="90%" style="padding-top:8px;"></h1>
                    <nav id="nav">
                        <ul>    
                            <li><a href="{{ url('login') }}" class="button">Sign In For Bussines</a></li>
                            <li><a href="{{ url('login') }}" class="button">Sign In For Customer</a></li>
                        </ul>
                    </nav>
                </header>

            <!-- Banner -->
                <section id="banner">
                    <center>
                        <div style="width:70%; margin-top:50px;">
                            <form>
                                <div class="row uniform">
                                    <div class="8u 12u(mobilep)">
                                        <input type="text" name="q" id="q" placeholder="Search city, location, restaurant, cafe or food" />
                                    </div>
                                    <div class="4u 12u(mobilep)">
                                        <input type="submit" value="Search" class="fit" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </center>
                </section>

            <!-- Main -->
                <section id="main" class="container">
                    <section class="box special">
                        <header class="major">
                            <p>Blandit varius ut praesent nascetur eu penatibus nisi risus faucibus nunc ornare<br />
                            adipiscing nunc adipiscing. Condimentum turpis massa.</p>
                        </header>
                        <div class="box">
                            <div class="container">

                            </div>
                        </div>
                    </section>
                </section>

            <!-- CTA -->
                <section id="cta">
                    <h2>Sign up for beta access</h2>
                    <p>Blandit varius ut praesent nascetur eu penatibus nisi risus faucibus nunc.</p>
                    <form>
                        <div class="row uniform 50%">
                            <div class="8u 12u(mobilep)">
                                <input type="email" name="email" id="email" placeholder="Email Address" />
                            </div>
                            <div class="4u 12u(mobilep)">
                                <input type="submit" value="Sign Up" class="fit" />
                            </div>
                        </div>
                    </form>

                </section>

            <!-- Footer -->
                <footer id="footer">
                    <ul class="icons">
                        <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
                        <li><a href="#" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
                        <li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
                        <li><a href="#" class="icon fa-github"><span class="label">Github</span></a></li>
                        <li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
                        <li><a href="#" class="icon fa-google-plus"><span class="label">Google+</span></a></li>
                    </ul>
                    <ul class="copyright">
                        <li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
                    </ul>
                </footer>

        </div>

        <!-- Scripts -->
            <script src="{{ url('packages/front/assets/js/jquery.min.js') }}"></script>
            <script src="{{ url('packages/front/assets/js/jquery.dropotron.min.js') }}"></script>
            <script src="{{ url('packages/front/assets/js/jquery.scrollgress.min.js') }}"></script>
            <script src="{{ url('packages/front/assets/js/skel.min.js') }}"></script>
            <script src="{{ url('packages/front/assets/js/util.js') }}"></script>
            <!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
            <script src="{{ url('packages/front/assets/js/main.js') }}"></script>

    </body>
</html>