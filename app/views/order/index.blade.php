<ol class="breadcrumb">
  <li><a href="{{ url('dashboard') }}">Dashboard</a></li>
  <li>{{ $title }}</li>
</ol>
<div class="row">
  <div class="col-lg-12">
    <div class="col-lg-12 well">
  		<form class="form-search" action="{{ url('item/search') }}" method="POST">
  			<div class="col-lg-3">
  				<div class="form-group">
  					<input type="text" class="form-control" placeholder="Invoice" name="q" id="q">
  				</div>
  			</div>
  			<div class="col-lg-3">
  				<div class="form-group">
  					<button type="submit" class="btn btn-info" id="search"><i class="fa fa-search"></i> Search</button>
  				</div>
  			</div>
  		</form>
    </div>
    <div class="col-lg-12">
  		@if($count > 0)
  			@if(Session::has('message'))
			    <div class="alert alert-success">
			    	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <center>{{ Session::get('message') }}</center>
			    </div>
			@endif
			<?php 
	        $page = $orders->getCurrentPage();
            if ($page == 1) {
            	$i = 0;
            } else {
            	$i = $orders->getFrom();
            }
            ?>
    		<table class="tablesorter" width="100%" cellpadding="2" cellspacing="2">
				<thead>
					<th width="4%"><center>#</center></th>
					<th width="10%"><center>Invoice</center></th>
					<th width="10%"><center>Subtotal</center></th>
					<th width="5%"><center>Tax</center></th>
					<th width="10%"><center>Total</center></th>
					<th width="10%"><center>Date</center></th>
					<th width="10%"><center>Payment Method</center></th>
					<th width="7%"><center>Status</center></th>
					<th width="5%"><center>Action</center></th>
				</thead>
				<tbody>
					@foreach($orders as $order) 
						<tr>
							<td><center>{{ ++$i }}</center></td>
							<td>{{ $order->invoice }}</td>
							<td align="right">{{ Money::rupiah($order->order_subtotal) }}</td>
							<td><center>{{ $order->order_tax }}</center></td>
							<td align="right">{{ Money::rupiah($order->order_total) }}</td>
							<td>{{ Date::getIndoHariJam($order->created_at) }}</td>
							<td><center>{{ $order->paymentmethod->payment_method_name }}</center></td>
							<td>
								@if ($item->order_statuses_id == 1)
									<center><span class="label label-warning">{{ $order->status->item_status_name }}</span></center>
								@elseif ($item->order_statuses_id == 2)
									<center><span class="label label-success">{{ $item->status->item_status_name }}</span></center>
								@elseif ($item->order_statuses_id == 3)
									<center><span class="label label-success">{{ $item->status->item_status_name }}</span></center>
								@elseif ($item->order_statuses_id == 4)
									<center><span class="label label-danger">{{ $item->status->item_status_name }}</span></center>
								@endif
							</td>
							<td><center><a href="{{ url('item/edit/'.$item->id.'') }}"><i class="fa fa-search"></i></a></center></td>
						</tr>
					@endforeach
				</tbody>
			</table>
			<ul class="pagination">
			{{ $orders->links() }}
			</ul>
		@else
		    <div class="alert alert-danger"><center>NO ORDER AVAILABLE</center></div>
	    @endif
	</div>
  </div>
</div>
