<ol class="breadcrumb">
  <li><a href="{{ url('dashboard') }}"><i class="mdi mdi-home"></i> Dashboard</a></li>
  <li><i class="mdi mdi-file"></i> Log Activity</li>
</ol>
<div class="row"> 
  <div class="col-lg-12">
  	<div class="col-lg-12 well">
  		<form class="form-search" action="{{ url('item/search') }}" method="POST">
  			<div class="col-lg-3">
  				<div class="form-group">
  					<input type="text" class="form-control" placeholder="Invoice" name="q" id="q">
  				</div>
  			</div>
  			<div class="col-lg-3">
  				<div class="form-group">
  					<button type="submit" class="btn btn-info" id="search"><i class="fa fa-search"></i> Search</button>
  				</div>
  			</div>
  		</form>
    </div>
  </div>
  <div class="col-lg-12">

	@if($count > 0) 
		<?php 
		$page = $logs->getCurrentPage();
		if ($page == 1) {
			$i = 0;
		} else {
			$i = $logs->getFrom();
		}
        ?>
		<table class="tablesorter" width="100%" cellpadding="3" cellspacing="3">
			<thead>
				<th width="5%"><center>#</center></th>
				<th width="30%"><center>Activity Name</center></th>
				<th width="20%"><center>Page</center></th>
				<th width="20%"><center>Date</center></th>
			</thead>
			<tbody>
				@foreach($logs as $val) 
					<tr>
						<td><center>{{ ++$i }}</center></td>
						<td>{{ $val->log_activity_name }}</td>
						<td>{{ $val->log_activity_route }}</td>
						<td>{{ $val->created_at }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
		<ul class="pagination">
			{{ $logs->links() }}
		</ul>
	@else
    	<div class="alert alert-danger"><center>NO LOG AVAILABLE</center></div>
    @endif
  </div>
</div>