<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>{{ $title }}</title>
	<meta name="description" content="">
	<meta name="author" content="Wikafe">
	<link rel="stylesheet" href="{{ url('packages/assets/css/bootstrap/bootstrap.css') }}" />
    <link href="{{ url('packages/bootflat/css/bootflat.css') }}" rel="stylesheet">
    <link href="{{ url('packages/assets/fonts/mdi/css/materialdesignicons.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ url('packages/assets/css/app/app.v1.css') }}" />
    <link rel="stylesheet" href="{{ url('packages/assets/js/plugins/pace/pace.css') }}" />
    <script src="{{ url('packages/assets/js/jquery/jquery.min.js') }}" type="text/javascript"></script>
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <aside class="left-panel">
        <div class="user text-center">
            <img src="{{ url('packages/img/login_logo.png') }}" class="img-circle" alt="{{ Session::get('username') }}">
            <h4 class="user-name">{{ Session::get('username') }}</h4>
        </div>
        <nav class="navigation">
            <ul class="list-unstyled">
                <li @if ($route == 'dashboard') class="active" @endif>
                	<a href="{{ url('dashboard') }}"><i class="mdi mdi-home"></i><span class="nav-label">Dashboard</span></a>
                </li>
                <li @if ($url == 'general') class="has-submenu active" @else class="has-submenu" @endif>
                	<a href="#"><i class="mdi mdi-settings"></i><span class="nav-label">General Setting</span></a>
                    <ul class="list-unstyled">
                        <li @if ($route == 'table') class="active" @endif>
                        	<a href="{{ url('table') }}"><i class="mdi mdi-chair-school"></i> Table</a>
                        </li>
                        <li @if ($route == 'employer') class="active" @endif>
                        	<a href="{{ url('employer') }}"><i class="mdi mdi-account"></i> Employer</a>
                        </li>
                        <li @if ($route == 'logactivity') class="active" @endif>
                        	<a href="{{ url('logactivity') }}"><i class="mdi mdi-file"></i> Log Activity</a>
                        </li>
                    </ul>
                </li>
                <li @if ($url == 'catalog') class="has-submenu active" @else class="has-submenu" @endif>
                	<a href="#"><i class="mdi mdi-content-paste"></i><span class="nav-label">Catalog</span></a>
                    <ul class="list-unstyled">
                        <li @if ($route == 'category') class="active" @endif>
                        	<a href="{{ url('category') }}"><i class="mdi mdi-folder-multiple"></i> Item's Category</a>
                        </li>
                        <li @if ($route == 'item') class="active" @endif>
                        	<a href="{{ url('item') }}"><i class="mdi mdi-food"></i> Item</a>
                        </li>
                    </ul>
                </li>
                <li @if ($route == 'order') class="active" @endif>
                	<a href="{{ url('order') }}"><i class="mdi mdi-silverware"></i><span class="nav-label">Order</span></a>
                </li>
                <li @if ($route == 'report') class="active" @endif>
                	<a href="{{ url('report') }}"><i class="mdi mdi-printer"></i><span class="nav-label">Report</span></a>
                </li>
                <li @if ($url == 'config') class="has-submenu active" @else class="has-submenu" @endif>
                	<a href="#"><i class="mdi mdi-settings-box"></i><span class="nav-label">Configuration</span></a>
                    <ul class="list-unstyled">
                        <li @if ($route == 'cafe') class="active" @endif>
                        	<a href="{{ url('cafe') }}"><i class="mdi mdi-store"></i> Manage Your Cafe</a>
                        </li>
                        <li @if ($route == 'account') class="active" @endif>
                        	<a href="{{ url('account') }}"><i class="mdi mdi-account"></i> Account Setting</a>
                        </li>
                        <li @if ($route == 'employer') class="active" @endif>
                        	<a href="{{ url('employer') }}"><i class="mdi mdi-cloud"></i> Services</a>
                        </li>
                        <li @if ($route == 'plan') class="active" @endif>
                        	<a href="{{ url('plan') }}"><i class="mdi mdi-credit-card"></i> Plan & Billing</a>
                        </li>
                    </ul>
                </li>
               <li><a href="{{ url('logout') }}"><i class="mdi mdi-power"></i><span class="nav-label">Log Out</span></a></li>
            </ul>
        </nav>
    </aside>
    <section class="content">
        <header class="top-head container-fluid">
            <button type="button" class="navbar-toggle pull-left">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </header>
        <div class="warper container-fluid">
            {{ $content }}
        </div>
        <footer class="container-fluid footer">
            Copyright &copy; {{ date('Y') }} <a href="http://wikafe.com/" target="_blank">Wikafe</a>
            <a href="#" class="pull-right scrollToTop"><i class="fa fa-chevron-up"></i></a>
        </footer>
    </section>
    <script src="{{ url('packages/assets/js/plugins/jquery-validate/jquery.validate.min.js" type="text/javascript"') }}'></script>
    <script src="{{ url('packages/assets/js/bootstrap/bootstrap.min.js') }}"></script>
    <script src="{{ url('packages/assets/js/globalize/globalize.min.js') }}"></script>
    <script src="{{ url('packages/assets/js/plugins/nicescroll/jquery.nicescroll.min.js') }}"></script>
    <script src="{{ url('packages/assets/js/app/custom.js" type="text/javascript"') }}'></script>
    <script src="{{ url('packages/assets/js/plugins/pace/pace.min.js" type="text/javascript"') }}'></script>
</body>
</html>