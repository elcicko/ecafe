<ol class="breadcrumb">
  <li><a href="{{ url('dashboard') }}"><i class="mdi mdi-home"></i> Dashboard</a></li>
  <li><i class="mdi mdi-account"></i> {{ $action_title }}</li>
</ol>
<script src="{{ asset('js_app/validation/account_form.js') }}"></script>
<div class="panel panel-info">
  <div class="panel-heading"><i class="mdi mdi-account"></i> <?php echo $action_title; ?></div>
  <div class="panel-body">
    <form class="form-vertical" id="form" method="POST" action="{{ $form_action }}">
    <fieldset>
        @if(Session::has('message'))
          <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <center>{{ Session::get('message') }}</center>
          </div>
        @elseif(Session::has('error'))
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <center>{{ Session::get('message') }}</center>
          </div>
        @endif

        <div class="col-lg-6">
            <div class="form-group">
              <label for="order">Username</label>  
              <input id="username" name="username" value="{{ isset($username) ? $username : '' }}" type="text" class="form-control" readonly="readonly">
            </div>

            <div class="form-group">
              <label for="order">Full Name</label>  
              <input id="fullname" name="fullname" value="{{ isset($fullname) ? $fullname : '' }}" type="text" class="form-control">
            </div>

            <div class="form-group">
              <label for="email">Email</label>  
              <input id="email" name="email" value="{{ isset($email) ? $email : '' }}" type="text" class="form-control">
            </div>

            <div class="form-group">
              <label for="order">Old Password</label>  
              <input id="oldpassword" name="oldpassword" type="password" class="form-control">
            </div>

            <div class="form-group">
              <label for="order">New Password</label>  
              <input id="password" name="password" type="password" class="form-control">
            </div>

            <div class="form-group">
              <label for="order">Confirm Password</label>  
              <input id="repassword" name="repassword" type="password" class="form-control">
            </div>

            <div class="form-group">
              <button id="save" name="save" class="btn btn-success"><i class="mdi mdi-content-save"></i> Save Changes</button>
            </div>
        </div>
    </fieldset>
    </form>
  </div>
</div>