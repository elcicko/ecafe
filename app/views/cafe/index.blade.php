<ol class="breadcrumb">
  <li><a href="{{ url('dashboard') }}"><i class="mdi mdi-home"></i> Dashboard</a></li>
  <li><i class="mdi mdi-store"></i> {{ $action_title }}</li>
</ol>
<script src="{{ asset('packages/assets/js/app/validation/cafe.js') }}"></script>
<script src="{{ asset('packages/assets/js/plugins/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('packages/assets/js/plugins/ckeditor/adapters/jquery.js') }}"></script>
<div class="panel panel-info">
  <div class="panel-heading"><i class="mdi mdi-store"></i> <?php echo $action_title; ?></div>
  <div class="panel-body">
    <form class="form-vertical" id="cafe" method="POST" action="{{ $form_action }}">
    <fieldset>
        @if(Session::has('message'))
            <div class="alert alert-success">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <center>{{ Session::get('message') }}</center>
            </div>
        @endif
        <div class="row">
          <div class="col-lg-6">
            <div class="form-group">
              <label for="order"><i class="mdi mdi-help-circle"></i> Naming Your Cafe</label>  
              <input id="cafe_name" name="cafe_name" value="{{ isset($cafe->cafe_name) ? $cafe->cafe_name : '' }}" type="text" placeholder="" class="form-control input-md">
            </div>

            <div class="form-group">
              <label for="order"><i class="mdi mdi-help-circle"></i> Describe Your Cafe</label>  
              <textarea id="cafe_description" name="cafe_description" rows="8" class="ckeditor form-control" style="resize: none;">{{ isset($cafe->cafe_description) ? $cafe->cafe_description : '' }}</textarea>
            </div>

            <div class="form-group"> 
              <label for="order"><i class="mdi mdi-map-marker"></i> Where is Your Cafe ?</label>  
              <input id="cafe_address" name="cafe_address" value="{{ isset($cafe->cafe_address) ? $cafe->cafe_address : '' }}" type="text" placeholder="" class="form-control input-md">
            </div>

            <div class="form-group">
              <label for="order"><i class="mdi mdi-map-marker"></i> What City Of Your Cafe ?</label>  
              <input id="cafe_city" name="cafe_city" value="{{ isset($cafe->cafe_city) ? $cafe->cafe_city : '' }}" type="text" placeholder="" class="form-control input-md">
            </div>

            <div class="form-group">
              <label for="order"><i class="mdi mdi-phone"></i> How Guest Contact Your Cafe ?</label>  
              <input id="cafe_phone_primary" name="cafe_phone_primary" value="{{ isset($cafe->cafe_phone_primary) ? $cafe->cafe_phone_primary : '' }}" type="text" placeholder="" class="form-control input-md">
            </div>

            <div class="form-group">
              <label for="order"><i class="mdi mdi-phone"></i> Is there Alternative To Contact Your Cafe ?</label>  
              <input id="cafe_phone_secondary" name="cafe_phone_secondary" value="{{ isset($cafe->cafe_phone_secondary) ? $cafe->cafe_phone_secondary : '' }}" type="text" placeholder="" class="form-control input-md">
            </div>

            <div class="form-group">
              <label for="order"><i class="mdi mdi-web"></i> Website</label>  
              <input id="cafe_website" name="cafe_website" value="{{ isset($cafe->cafe_website) ? $cafe->cafe_website : '' }}" type="text" placeholder="" class="form-control input-md">
            </div>

            <div class="form-group">
              <label for="order"><i class="mdi mdi-facebook"></i> Facebook</label>  
              <input id="cafe_facebook" name="cafe_facebook" value="{{ isset($cafe->cafe_facebook) ? $cafe->cafe_facebook : '' }}" type="text" placeholder="" class="form-control input-md">
            </div>

            <div class="form-group">
              <label for="order"><i class="mdi mdi-twitter"></i> Twitter</label>  
              <input id="cafe_twitter" name="cafe_twitter" value="{{ isset($cafe->cafe_twitter) ? $cafe->cafe_twitter : '' }}" type="text" placeholder="" class="form-control input-md">
            </div>

            <div class="form-group">
              <label for="order"><i class="mdi mdi-instagram"></i> Instagram</label>  
              <input id="cafe_instagram" name="cafe_instagram" value="{{ isset($cafe->cafe_instagram) ? $cafe->cafe_instagram : '' }}" type="text" placeholder="" class="form-control input-md">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-6">
              <div class="form-group">
                  <br>
                  <button id="save" name="save" class="btn btn-success"><i class="fa fa-save"></i> SAVE CHANGES</button>
              </div>
          </div>
        </div>
      </fieldset>
    </form>
  </div>
</div>