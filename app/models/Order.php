<?php

class Order extends \Eloquent {
	
	protected $table = 'orders';

	public function cafe() {
		return $this->belongsTo('Cafe', 'cafe_id');
	}

	public function paymentmethod() {
		return $this->belongsTo('PaymentMethod', 'payment_method_id');
	}

	public function status() {
		return $this->belongsTo('OrderStatus', 'order_statuses_id');
	}

	public function scopeSearchInvoice($query, $q) {
	  if ($q) return $query->where('order_invoice','LIKE', '%'.$q.'%');
	}

	public function scopeSearchProvider($query, $provider) {
		if ($provider) return $query->where('provider_id', $provider);
	}

	public function scopeSearchPaymentMethod($query, $paymentmethod) {
		if ($paymentmethod) return $query->where('payment_method_id', $paymentmethod);
	}

	public function scopeSearchStatus($query, $status) {
		if ($status) return $query->where('status_id', $status);
	}

	public function scopeSearchNominal($query, $nominal) {
		if ($nominal) return $query->where('nominal', $nominal);
	}
}