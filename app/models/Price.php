<?php

class Price extends \Eloquent {
	protected $table = 'prices';
	public $timestamps = false;

	public function provider() {
		return $this->belongsTo('Provider', 'provider_id');
	}

	public function scopeSearchProvider($query, $provider) {
		if ($provider) return $query->where('provider_id', $provider);
	}

	public function scopeSearchNominal($query, $nominal) {
		if ($nominal) return $query->where('provider_id', $nominal);
	}
}