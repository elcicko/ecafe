<?php

class PaymentMethod extends \Eloquent {
	protected $table = 'payment_methods';
	public $timestamps = false;
}