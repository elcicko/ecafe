<?php

class Item extends \Eloquent {

	protected $table = 'items';

	public function cafe() {
		return $this->belongsTo('Cafe','cafe_id');
	}

	public function category() {
		return $this->belongsTo('Category','category_id');
	}

	public function status() {
		return $this->belongsTo('ItemStatus','item_status_id');
	}

	public static function get_pic($picture) {
		$result = '';
		if ($picture != "") {
			$result = '<img src="'.url($picture).'">';
		} else {
			$result = '<img src="'.url('packages/img/icon/item.png').'">';
		}
		return $result;
	}

	public function scopeSearchByCafe($query, $cafe) {
		if ($cafe) return $query->where('cafe_id', $cafe);
	}

	public function scopeSearchItem($query, $q) {
		if ($q) return $query->where('item_name','LIKE', '%'.$q.'%');
	}

	public function scopeSearchCategory($query, $category) {
		if ($category) return $query->where('category_id', $category);
	}

	public function scopeSearchStatus($query, $status) {
		if ($status) return $query->where('item_status_id', $status);
	}
}