<?php

class Table extends \Eloquent {

	protected $table = 'tables';

	public function cafe() {
		return $this->belongsTo('Cafe', 'cafe_id');
	}

	public function scopeSearchAvailable($query, $status) {
		if ($status) return $query->where('status',$status);
	}

	public function scopeSearchByCafe($query, $cafe) {
		if ($cafe) return $query->where('cafe_id', $cafe);
	}

}
