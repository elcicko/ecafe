<?php

class LogActivity extends Eloquent 
{

	protected $table = 'log_activities';

	public function user() {
		return $this->belongsTo('User');
	}

}