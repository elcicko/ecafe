<?php

class Category extends \Eloquent {

	protected $table = 'categories';

	public function cafe() {
		return $this->belongsTo('Cafe','cafe_id');
	}

	public function scopeSearchByCafe($query, $cafe) {
		if ($cafe) return $query->where('cafe_id',$cafe);
	}

	public function scopeSearchCategory($query, $q) {
		if ($q) return $query->where('category_name','LIKE', '%'.$q.'%');
	}

}
