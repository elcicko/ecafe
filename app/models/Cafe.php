<?php

class Cafe extends \Eloquent {

	protected $table = 'cafes';

	public function owner() {
		return $this->hasOne('Owner', 'owner_id');
	}
	
}
