<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Employer extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	protected $table = 'employers';
	protected $hidden = array('password');
	public $result;

	public function level() {
		return $this->belongsTo('Level','level_id');
	}

	public function cafe() {
		return $this->belongsTo('Cafe', "cafe_id");
	}

	public static function get_pic($activated, $level_id) {

		$result = '';

		if ($activated == "yes" && $level_id == '1') {
			$result = '<img src="'.url('packages/img/icon/waitress-available.png').'">';
		}
		if ($activated == "no" && $level_id == '1') {
			$result = '<img src="'.url('packages/img/icon/waitress-unavailable.png').'">';
		}

		if ($activated == "yes" && $level_id == '2') {
			$result = '<img src="'.url('packages/img/icon/kitchen-available.png').'">';
		}

		if ($activated == "no" && $level_id == '2') {
			$result = '<img src="'.url('packages/img/icon/kitchen-unavailable.png').'">';
		}

		if ($activated == "yes" && $level_id == '3') {
			$result = '<img src="'.url('packages/img/icon/cashier-available.png').'">';
		}

		if ($activated == "no" && $level_id == '3') {
			$result = '<img src="'.url('packages/img/icon/cashier-unavailable.png').'">';
		}

		return $result;
	}

	public function scopeSearchByCafe($query, $cafe) {
		if ($cafe) return $query->where('cafe_id',$cafe);
	}

	public function scopeSearchName($query, $q) {
		if ($q) return $query->where('employer_name','LIKE', '%'.$q.'%');
	}

	public function scopeSearchStatus($query, $status) {
		if ($status) return $query->where('employer_activated', $status);
	}

	public function scopeSearchLevel($query, $level) {
	  if ($level) return $query->where('level_id', $level);
	}

	public function getAuthIdentifier() {
		return $this->getKey();
	}

	public function getAuthPassword() {
		return $this->password;
	}

}
