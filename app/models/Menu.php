<?php

class Menu extends Eloquent 
{
	protected $table = 'menus';

	public function menutype() {
		return $this->belongsTo('MenuType','menu_type_id');
	}

	public function scopeSearchNameRoute($query, $q) {
	  if ($q) return $query->where('menu_name','LIKE', '%'.$q.'%')->orwhere('menu_route','LIKE', '%'.$q.'%');
	}

	public function scopeSearchMenuType($query, $menutype) {
	  if ($menutype) return $query->where('menu_type_id',$menutype);
	}
}