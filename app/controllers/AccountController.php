<?php

class AccountController extends \BaseController
{

	public $layout 	= 'layouts.default';
	public $title  	= 'Account Setting';
	public $route	= 'account';

	public function __construct() {
		$this->beforeFilter('@filterRequest');
	}

	public function filterRequest() {
		if (Session::has('login')) {

		} else {
			return Redirect::to('/');
		}
	}

	public function index() {
		$this->layout->title = $this->title;
		$this->layout->url   = 'config';
		$this->layout->route = 'account';
		$view = View::make('account.index');

		$id 	  		= Session::get('owner_id');
		$username 	= Session::get('username');
      $fullname 	= Session::get('fullname');
      $email    	= Session::get('email');
      $password 	= Session::get('password');

		$view->form_action = url('account/update');
		$view->action_title = 'Account Setting';

		$view->id = $id;
		$view->fullname = $fullname;
		$view->username = $username;
		$view->email 	= $email;
		$view->password = $password;

		$this->layout->content = $view;
	}

	public function updateaccount() {
        $fullname 	 		= Input::get('fullname');
        $password 	 		= Input::get('password');
        $oldpassword 		= Input::get('oldpassword');
        $password_session 	= Session::get('password');
        $id = Session::get('owner_id');

		  if (md5($oldpassword) == $password_session) {
			$owner = Owner::find($id);
			$owner->fullname = $fullname;
			$owner->password = md5($password);
			$owner->save();
			Session::forget('password');
			Session::forget('fullname');
			Session::put('password', md5($password));
			Session::put('fullname', $fullname);
        	return Redirect::to('account')->with('message', 'Your Account is Successfully Updated!');
        } else {
        	return Redirect::to('account')->with('error', 'Password is Not Matched!');
        }
  	}
}
