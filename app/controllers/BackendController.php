<?php

class BackendController extends \BaseController
{
	public $layout = 'layouts.default';
	public $title	= 'Dashboard';
	public $route	= 'dashboard';
	public $cafe;

	public function __construct() {
		$this->beforeFilter('@filterRequest');
	}

	public function filterRequest() {
		if (Session::has('login')) {
			$this->cafe = Session::get('cafe');
		} else {
			Auth::logout();
        	Session::flush();
			return Redirect::to('/');
		}
	}

	public function index() {
		$this->layout->title = $this->title;
		$this->layout->url = 'dashboard';
		$view = View::make('dashboard.index');
		$order = Order::where('cafe_id', $this->cafe)->take(10);
		$count = Order::where('cafe_id', $this->cafe)->count();
		$view->order = $order;
		$view->count = $count;
		$this->layout->route = $this->route;
		$this->layout->content = $view;
	}
}
