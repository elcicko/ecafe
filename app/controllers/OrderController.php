<?php

class OrderController extends \BaseController 
{
	public $layout 	= 'layouts.default';
	public $title   = 'List Of Orders';
	public $route   = 'order';
	public $cafe;

	public function __construct() {
		$this->beforeFilter('@filterRequest');
	}

	public function filterRequest() {
		if (Session::has('login')) {
			$this->cafe = Session::get('cafe');
		} else {
			return Redirect::to('/login');
		}
	}

	public function index() {
		if (Session::has('login')) {
			$this->layout->title = $this->title;
			$this->layout->url = 'order';	
			$this->layout->route = $this->route;	
			$view = View::make('order.index');
			$view->title = $this->title;
			$view->orders = Order::where('cafe_id', $this->cafe)->paginate(30);
			$view->count  = Order::where('cafe_id', $this->cafe)->count();
			$this->layout->content = $view;
		} else {
			return Redirect::to('/login');
		}
	}
}