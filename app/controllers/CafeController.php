<?php

class CafeController extends \BaseController 
{

	public $layout 	= 'layouts.default';
	public $title  	= 'Manage Your Cafe';
	public $route	= 'cafe';
	public $cafe;

	public function __construct() {
		$this->beforeFilter('@filterRequest');
	}

	public function filterRequest() {
		if (Session::has('login')) {
			$this->cafe = Session::get('cafe');
		} else {
			return Redirect::to('/');
		}
	}
	
	public function index() {
		$this->layout->title = $this->title;
		$this->layout->url 	 = 'config';
		$this->layout->route = $this->route;	
		$view = View::make('cafe.index');
		$cafe = Cafe::find($this->cafe);
		$view->cafe = $cafe;
		$view->form_action = url('cafe/update/'.$cafe->id);
		$view->form_title = 'edit';
		$view->action_title = 'Manage Your Cafe';
		$this->layout->content = $view;
	}

	public function update($id) {
		$cafe = Cafe::find($id);
		$cafe->cafe_name 			= Input::get('cafe_name');
		$cafe->cafe_description 	= Input::get('cafe_description');
		$cafe->cafe_address			= Input::get('cafe_address');
		$cafe->cafe_city			= Input::get('cafe_city');
		$cafe->cafe_phone_primary	= Input::get('cafe_phone_primary');
		$cafe->cafe_phone_secondary	= Input::get('cafe_phone_secondary');
		$cafe->cafe_website 		= Input::get('cafe_website');
		$cafe->cafe_twitter 		= Input::get('cafe_twitter');
		$cafe->cafe_facebook 		= Input::get('cafe_facebook');
		$cafe->cafe_instagram 		= Input::get('cafe_instagram');
		$cafe->save();
		return Redirect::to('cafe')->with('message', 'ALL CHANGES HAS SUCCESSFULLY SAVED');
    }
}