<?php

class CategoryController extends BaseController
{
	public $layout = 'layouts.default';
	public $title  = "Manage Your Item's Category";
	public $route  = 'category';
	public $cafe;

	public function __construct() {
		$this->beforeFilter('@filterRequest');
	}

	public function filterRequest() {
		if (Session::has('login')) {
			$this->cafe = Session::get('cafe');
		} else {
			return Redirect::to('/');
		}
	}

	public function index() {
		$this->layout->title = $this->title;
		$this->layout->url   = 'catalog';
		$this->layout->route = $this->route;
		$view = View::make('category.index');
		$view->form_action = url('category/search');
		$view->title = $this->title;
		$view->categories = Category::where('cafe_id', $this->cafe)->paginate(20);
		$view->count 		= Category::where('cafe_id', $this->cafe)->count();
		$this->layout->content = $view;
	}

	public function search() {
		$this->layout->title = $this->title;
		$this->layout->url   = 'catalog';
		$this->layout->route = $this->route;
		$view = View::make('category.index');
		$view->form_action = url('category/search');
		$view->title = $this->title;

		$q	= Input::get('q');

		$categories  = Category::SearchByCafe($this->cafe)->SearchCategory($q)->paginate(20);
		$view->count = Category::SearchByCafe($this->cafe)->SearchCategory($q)->count();

		$view->categories = $categories;
		$this->layout->content = $view;
	}

	public function create() {
		$this->layout->title = $this->title;
		$this->layout->url   = 'catalog';
		$this->layout->route = $this->route;
		$view = View::make('category.form');
		$view->title = $this->title;
		$view->form_action = url('category/store');
		$view->form_title = 'add';
		$view->action_title = 'Add Category';
		$this->layout->content = $view;
	}

	public function store() {
		if (Input::hasFile('category_picture')) {
			$img = Image::make($_FILES['category_picture']['tmp_name']);
			$img_name = $_FILES['category_picture']['name'];

			$img->resize(726, null, function ($constraint) {
			    $constraint->aspectRatio();
			});

			$img->resize(null, 600, function ($constraint) {
			    $constraint->aspectRatio();
			});

			$dir = 'pic/'.$this->cafe.'/category';

			if (File::exists($dir)) {
			} else {
				File::makeDirectory($dir, 0777, true);
			}

			$path_dir = $dir.'/'.$img_name;
			$img->save($path_dir);
			$category = new Category;
			$category->category_name 	 = Input::get('category_name');
			$category->category_picture = $path_dir;
			$category->cafe_id 			 = $this->cafe;
			$category->save();

		} else {
			$category = new Category;
			$category->category_name = Input::get('category_name');
			$category->cafe_id 		 = $this->cafe;
			$category->save();
		}

		return Redirect::to('category')->with('message', 'Category is Successfully Saved');
	}

	public function edit($id) {
		$this->layout->title = $this->title;
		$this->layout->url   = 'catalog';
		$this->layout->route = $this->route;
		$view = View::make('category.form');
		$view->title = $this->title;
		$view->form_action = url('category/update/'.$id.'');
		$view->action_title = 'Update Category';
		$view->form_title = 'edit';
		$view->category = Category::find($id);
		$this->layout->content = $view;
		Session::flash('category_id', $id);
	}

	public function update($id) {
		$category = Category::find($id);
		$category->category_name 	= Input::get('category_name');
		$category->save();
		return Redirect::to('category')->with('message', 'Category is Successfully Updated');
	}

	public function destroy($id) {
		$category = Category::find($id);
		$items = Item::where('category_id',$category->id)->get();
		if ($category->category_picture != '') { File::delete($category->category_picture); }
		foreach ($items as $item) {
			if ($item->item_picture != '') { 
				File::delete($item->item_picture); 
			}
		}
		$category->delete();
		return Redirect::to('category')->with('message', 'Category Is Successfully Removed');
	}
}
