<?php

class LoginController extends BaseController 
{
	public function __construct() {
		$this->beforeFilter('csrf', array('on'=>'post'));
	}

	public function index() {
		return View::make('login', array('form_action' => url('validate')));
	}

	public function validate() {
        $data = array();
        $user_data['username'] = Input::get('username');
        $user_data['password'] = md5(Input::get('password'));
        $owner = Owner::where($user_data)->first();
        if ($owner) {
        	$cafe = Cafe::where('owner_id', $owner->id)->firstOrFail();
	        Session::put('owner_id', $owner->id);
	        Session::put('fullname', $owner->fullname);
	        Session::put('username', $owner->username);
	        Session::put('email', $owner->email);
	        Session::put('password', $owner->password);
	        Session::put('plan', $owner->plan_id);
	        Session::put('cafe', $cafe->id);
	        Session::put('login', TRUE);
        	return Redirect::to('dashboard');
        } else {
        	return Redirect::to('login')->with('error', 'Username or Password is not matched!')->withInput();
        }	
    }

    public function logout() {
    	Auth::logout();
        Session::flush();
        return Redirect::to('login');
    }
}