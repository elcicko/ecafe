<?php

class ItemController extends BaseController 
{

	public $layout 	= 'layouts.default';
	public $title 	= 'Managing Your Item';
	public $route 	= 'item';
	public $cafe;

	public function __construct() {
		$this->beforeFilter('@filterRequest');
	}

	public function filterRequest() {
		if (Session::has('login')) {
			$this->cafe = Session::get('cafe');
		} else {
			return Redirect::to('/');
		}
	}

	public function index() {
		$this->layout->title = $this->title;
		$this->layout->url 	 = 'catalog';	
		$this->layout->route = $this->route; 
		$view = View::make('item.index');
		$view->form_action = url('item/search');
		$view->title = $this->title;
		$view->items = Item::where('cafe_id', $this->cafe)->paginate(20);
		$view->count = Item::where('cafe_id', $this->cafe)->count();

		$categories = Category::where('cafe_id', $this->cafe)->get();
		$category = array();
		
		if (count($categories) > 0) {
			foreach ($categories as $val) {
				$category['']   = "Choose Category";
				$category[$val->id] = $val->category_name; 
			}
		} else {
			$category['']   = "You Have No Category";
		}

		$statuses = ItemStatus::all();
		$status = array();
		
		foreach ($statuses as $val) {
			$status['']   = 'Choose Item Status';
			$status[$val->id] = $val->item_status_name; 
		}

		$view->category = $category;
		$view->status   = $status;
		$this->layout->content = $view;
	}

	public function search() {
		$this->layout->title = $this->title;
		$this->layout->url 	 = 'catalog';	
		$this->layout->route = $this->route; 
		$view = View::make('item.index');
		$view->form_action = url('item/search');
		$view->title = $this->title;

		$stat = Input::get('status');
		$cat  = Input::get('category');
		$q	  = Input::get('q');

		$view->items = Item::SearchByCafe($this->cafe)->SearchItem($q)->SearchCategory($cat)->SearchStatus($stat)->paginate(20);
		$view->count = Item::SearchByCafe($this->cafe)->SearchItem($q)->SearchCategory($cat)->SearchStatus($stat)->count();

		$categories = Category::where('cafe_id', $this->cafe)->get();
		$category = array();
		
		if (count($categories) > 0) {
			foreach ($categories as $val) {
				$category['']   = "Choose Category";
				$category[$val->id] = $val->category_name; 
			}
		} else {
			$category['']   = "You Have No Category";
		}

		$statuses = ItemStatus::all();
		$status = array();
		
		foreach ($statuses as $val) {
			$status['']   = 'Choose Item Status';
			$status[$val->id] = $val->item_status_name; 
		}

		$view->category = $category;
		$view->status   = $status;
		$this->layout->content = $view;
	}

	public function create() {
		$this->layout->title = $this->title;
		$this->layout->url 	 = 'catalog';	
		$this->layout->route = $this->route; 
		$view = View::make('item.form');
		$view->title = $this->title;
		$view->form_action = url('item/store');

		$categories = Category::where('cafe_id', $this->cafe)->get();
		$category = array();
		
		if (count($categories) > 0) {
			foreach ($categories as $val) {
				$category['']   = "Choose Category";
				$category[$val->id] = $val->category_name; 
			}
		} else {
			$category['']   = "You Have No Category";
		}

		$statuses = ItemStatus::all();
		$status = array();
		
		foreach ($statuses as $val) {
			$status['']   = 'Choose Item Status';
			$status[$val->id] = $val->item_status_name; 
		}

		$view->category = $category;
		$view->status   = $status;

		$view->form_title = 'add';
		$view->action_title = 'Add New Item';
		$this->layout->content = $view;
	}

	public function store() {
		if (Input::hasFile('picture')) {
			$img = Image::make($_FILES['picture']['tmp_name']);
			$img_name = $_FILES['picture']['name'];

			$img->resize(726, null, function ($constraint) {
			    $constraint->aspectRatio();
			});

			$img->resize(null, 600, function ($constraint) {
			    $constraint->aspectRatio();
			});

			$dir = 'pic/'.$this->cafe.'/item';

			if (File::exists($dir)) {
			} else {
				File::makeDirectory($dir, 0777, true);
			}

			$path_dir = $dir.'/'.$img_name;
			$img->save($path_dir);

			$item = new Item;
			$item->item_name 	  	= Input::get('name');
			$item->category_id 	  	= Input::get('category');
			$item->item_status_id 	= Input::get('status');
			$item->item_description	= Input::get('description');
			$item->item_price 		= Input::get('price');
			$item->item_discount 	= Input::get('discount');
			$item->item_picture		= $path_dir;
			$item->cafe_id 		  	= $this->cafe;
			$item->save();		
		} else {
			$item = new Item;
			$item->item_name 	  	= Input::get('name');
			$item->category_id 	  	= Input::get('category');
			$item->item_status_id 	= Input::get('status');
			$item->item_description	= Input::get('description');
			$item->item_price 		= Input::get('price');
			$item->item_discount 	= Input::get('discount');
			$item->cafe_id 		  	= $this->cafe;
			$item->save();		
		}
		return Redirect::to('item')->with('message', 'Item Has Successfully Saved');
	}

	public function edit($id) {
		$this->layout->title = $this->title;
		$this->layout->url 	 = 'catalog';	
		$this->layout->route = $this->route; 
		$view = View::make('item.form');
		$view->title = $this->title;
		$view->form_action = url('item/update/'.$id.'');
		$view->action_title = 'Update Your Item';
		$view->form_title = 'edit';
		$view->item = Item::find($id);

		$categories = Category::where('cafe_id', $this->cafe)->get();
		$category = array();
		
		if (count($categories) > 0) {
			foreach ($categories as $val) {
				$category['']   = "Choose Category";
				$category[$val->id] = $val->category_name; 
			}
		} else {
			$category['']   = "You Have No Category";
		}

		$statuses = ItemStatus::all();
		$status = array();
		
		foreach ($statuses as $val) {
			$status['']   = 'Choose Item Status';
			$status[$val->id] = $val->item_status_name; 
		}

		$view->category = $category;
		$view->status   = $status;
		
		$this->layout->content = $view;
		Session::flash('item_id', $id);
	}
	
	public function update($id) {
		$item = Item::find($id);
		$item->item_name 	  = Input::get('name');
		$item->category_id 	  = Input::get('category');
		$item->item_status_id = Input::get('status');
		$item->price 		  = Input::get('price');
		$item->save();
		return Redirect::to('item')->with('message', 'ITEM IS SUCCESSFULLY UPDATED');
	}
	
	public function destroy($id) {
		$item = Item::find($id);
		if ($item->item_picture != '') { File::delete($item->item_picture); }
		$item->delete();
		return Redirect::to('item')->with('message', 'ITEM IS SUCCESSFULLY REMOVED');
	}
}