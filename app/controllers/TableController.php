<?php

class TableController extends BaseController
{
	public $layout  = 'layouts.default';
	public $route   = 'table';
	public $title	 = 'Manage Your Table';
	public $cafe;

	public function __construct() {
		$this->beforeFilter('@filterRequest');
	}

	public function filterRequest() {
		if (Session::has('login')) {
			$this->cafe = Session::get('cafe');
		} else {
			return Redirect::to('/');
		}
	}

	public function index() {
		$this->layout->title = $this->title;
		$this->layout->url 	 = 'general';
		$this->layout->route = $this->route;
		$view = View::make('table.index');
		$view->form_action = url('table/search');
		$view->title = $this->title;
		$view->tables = Table::where('cafe_id', $this->cafe)->paginate(20);
		$view->count = Table::where('cafe_id', $this->cafe)->count();

		$statuses = array('Unavailable'=>'Unavailable','Available'=>'Available');
		$status = array();

		foreach ($statuses as $key=>$var) {
			$status['']   = 'Choose Status';
			$status[$key] = $var;
		}

		$view->status = $status;
		$this->layout->content = $view;
	}

	public function search() {
		$this->layout->title = $this->title;
		$this->layout->url 	 = 'general';
		$this->layout->route = $this->route;
		$view = View::make('table.index');
		$view->form_action = url('table/search');
		$view->title = $this->title;

		$table_status = Input::get('status');

		$view->tables = Table::searchByCafe($this->cafe)->searchAvailable($table_status)->paginate(20);
		$view->count  = Table::searchByCafe($this->cafe)->searchAvailable($table_status)->count();

		$statuses = array('Unavailable'=>'Unavailable','Available'=>'Available');
		$status 	 = array();

		foreach ($statuses as $key=>$var) {
			$status['']   = 'Choose Status';
			$status[$key] = $var;
		}

		$view->status = $status;
		$this->layout->content = $view;
	}

	public function create() {
		$this->layout->title = $this->title;
		$this->layout->url 	 = 'general';
		$this->layout->route = $this->route;
		$view = View::make('table.form');
		$view->title = $this->title;
		$view->form_action = url('table/store');
		$view->form_title = 'add';
		$view->action_title = 'Add New Table';

		$statuses = array('Unavailable'=>'Unavailable','Available'=>'Available');
		$status = array();

		foreach ($statuses as $key=>$var) {
			$status['']   = 'Choose Status';
			$status[$key] = $var;
		}

		$view->status = $status;
		$this->layout->content = $view;
	}

	public function store() {
		$table = new Table;
		$table->table_number 	= Input::get('table_number');
		$table->table_desc 		= Input::get('table_desc');
		$table->status 			= Input::get('status');
		$table->cafe_id 			= $this->cafe;
		$table->save();
		return Redirect::to('table')->with('message', 'Table Has Successfully Saved');
	}

	public function edit($id) {
		$this->layout->title = $this->title;
		$this->layout->url 	 = 'general';
		$this->layout->route = $this->route;
		$view = View::make('table.form');
		$view->title = $this->title;
		$view->form_action = url('table/update/'.$id.'');
		$view->action_title = 'Update Table';
		$view->form_title = 'edit';
		$view->table = Table::find($id);

		$statuses = array('Unavailable'=>'Unavailable','Available'=>'Available');
		$status = array();

		foreach ($statuses as $key=>$var) {
			$status['']   = 'Choose Status';
			$status[$key] = $var;
		}

		$view->status = $status;
		$this->layout->content = $view;
		Session::flash('table_id', $id);
	}

	public function update($id) {
		$table = Table::find($id);
		$table->table_number = Input::get('table_number');
		$table->status 		= Input::get('status');
		$table->table_desc 	= Input::get('table_desc');
		$table->save();
		return Redirect::to('table')->with('message', 'Table Has Successfully Updated');
	}

	public function destroy($id) {
		$table = Table::find($id);
		$table->delete();
		return Redirect::to('table')->with('message', 'Table Has Successfully Removed');
	}
}
