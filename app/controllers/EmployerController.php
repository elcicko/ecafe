<?php

class EmployerController extends \BaseController
{

	public $layout = 'layouts.default';
	public $title  = 'Manage Your Employer';
	public $route	= 'employer';
	public $cafe;

	public function __construct() {
		$this->beforeFilter('@filterRequest');
	}

	public function filterRequest() {
		if (Session::has('login')) {
			$this->cafe = Session::get('cafe');
		} else {
			return Redirect::to('/');
		}
	}

	public function index() {
		$this->layout->title = $this->title;
		$this->layout->url   = 'general';
		$this->layout->route = $this->route;
		$view = View::make('employer.index');
		$view->title = $this->title;
		$employers 	 = Employer::where('cafe_id', $this->cafe)->paginate(20);
		$view->count = Employer::where('cafe_id', $this->cafe)->count();

		$levels = Level::all();
		$level = array();

		foreach ($levels as $val) {
			$level[''] = "Choose Employer's Level";
			$level[$val->id] = $val->level_name;
		}

		$statuses = array('yes'=>'Active','no'=>'Non Active');
		$status = array();

		foreach ($statuses as $key=>$val) {
			$status[''] = "Choose Employer's Status";
			$status[$key] = $val;
		}

		$view->employers 	= $employers;
		$view->level  	 	= $level;
		$view->status 	 	= $status;
		$this->layout->content = $view;
	}

	public function search() {
		$this->layout->title = $this->title;
		$this->layout->url   = 'general';
		$this->layout->route = $this->route;
		$view = View::make('employer.index');
		$view->title = $this->title;

		$q		  	  = Input::get('q');
		$level_id  = Input::get('level');
		$status	  = Input::get('status');

		$employers 	 = Employer::SearchByCafe($this->cafe)->SearchName($q)->searchLevel($level_id)->searchStatus($status)->paginate(20);
		$view->count = Employer::SearchByCafe($this->cafe)->SearchName($q)->searchLevel($level_id)->searchStatus($status)->count();

		$levels = Level::all();
		$level = array();

		foreach ($levels as $val) {
			$level[''] = 'Choose Level';
			$level[$val->id] = $val->level_name;
		}

		$statuses = array('yes'=>'Active','no'=>'Non Active');
		$status = array();

		foreach ($statuses as $key=>$val) {
			$status[''] = 'Choose Status';
			$status[$key] = $val;
		}

		$view->employers  = $employers;
		$view->level  		= $level;
		$view->status 		= $status;
		$this->layout->content = $view;
	}

	public function create() {
		$this->layout->title = $this->title;
		$this->layout->url   = 'general';
		$this->layout->route = $this->route;
		$view = View::make('employer.form');
		$view->form_action = url('employer/store');
		$view->action_title = 'Add Employer';
		$view->title = $this->title;

		$levels = Level::all();
		$level = array();

		foreach ($levels as $val) {
			$level[''] = 'Choose Level';
			$level[$val->id] = $val->level_name;
		}

		$statuses = array('yes'=>'Active','no'=>'Non Active');
		$status = array();

		foreach ($statuses as $key=>$val) {
			$status[''] = "Choose Employer's Status";
			$status[$key] = $val;
		}

		$view->status 	= $status;
		$view->level 	= $level;
		$this->layout->content = $view;
	}

	public function store() {
		$checkuser = Employer::where('employer_email', Input::get('employer_email'))->count();
		if ($checkuser == 0) {
			$employer = new Employer;
			$employer->employer_name 		= Input::get('employer_name');
			$employer->employer_email 		= Input::get('employer_email');
			$employer->employer_password 	= md5('employer');
			$employer->level_id 				= Input::get('level');
			$employer->employer_activated	= Input::get('status');
			$employer->cafe_id 				= $this->cafe;
			$employer->save();
			return Redirect::to('employer')->with('message', 'Employer Has Successfuly Saved');
		} else {
			return Redirect::to('employer/create')->with('message', 'Someone Has Been Registered With That Email Before');
		}
	}

	public function edit($id) {
		$this->layout->title = $this->title;
		$this->layout->url   = 'general';
		$this->layout->route = $this->route;
		$view = View::make('employer.form');
		$view->form_action = url('employer/update/'.$id.'');
		$view->action_title = 'Edit Employer';
		$view->employer = Employer::find($id);
		$view->title = $this->title;

		$levels = Level::all();
		$level = array();

		foreach ($levels as $val) {
			$level[''] = 'Choose Level';
			$level[$val->id] = $val->level_name;
		}

		$statuses = array('yes'=>'Active','no'=>'Non Active');
		$status = array();

		foreach ($statuses as $key=>$val) {
			$status[''] = "Choose Employer's Status";
			$status[$key] = $val;
		}

		$view->status 	= $status;
		$view->level 	= $level;
		$this->layout->content = $view;
		Session::flash('user_id', $id);
	}

	public function update($id) {
		$employer = Employer::find($id);
		$employer->employer_name 		= Input::get('employer_name');
		$employer->employer_email 		= Input::get('employer_email');
		$employer->level_id 				= Input::get('level');
		$employer->employer_activated	= Input::get('status');
		$employer->save();
		return Redirect::to('employer')->with('message', 'Employer Has Successfuly Updated');
	}

	public function destroy($id) {
		$employer = Employer::find($id);
		$employer->delete();
		return Redirect::to('employer')->with('message', 'Employer Has Successfuly Removed');
	}
}
