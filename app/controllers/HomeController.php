<?php

class HomeController extends BaseController {

	public function index() {
		return View::make('home');
	}

	public function search() {
		return View::make('search');
	}

}
