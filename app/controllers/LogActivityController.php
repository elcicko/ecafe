<?php

class LogActivityController extends BaseController 
{

	public $layout 	= 'layouts.default';
	public $title  	= 'Log Activities';
	public $route 	= 'logactivity';
	public $cafe;

	public function __construct() {
		$this->beforeFilter('@filterRequest');
	}

	public function filterRequest() {
		if (Session::has('login')) {
			$this->cafe = Session::get('cafe');
		} else {
			return Redirect::to('/');
		}
	}

	public function index() {
		$this->layout->title = $this->title;
		$this->layout->url   = 'general';	
		$this->layout->route = $this->route;	
		$view = View::make('log.index');
		$view->logs  = LogActivity::where('cafe_id', $this->cafe)->paginate(50);
		$view->count = LogActivity::where('cafe_id', $this->cafe)->count();
		$this->layout->content = $view;
	}

	public function search() {
		$this->layout->title = $this->title;
		$this->layout->url   = 'general';	
		$this->layout->route = $this->route;
		$view = View::make('log.index');
		$view->form_action = url('log/search');

		$user = Input::get('user');
		$route = Input::get('route');
		$q = Input::get('q');

		$view->logs  = LogAktifitas::searchActivity($q)->searchUser($user)->searchRoute($route)->paginate(50);
		$view->count = LogAktifitas::searchActivity($q)->searchUser($user)->searchRoute($route)->count();

		$routes = DB::table('log_aktifitas')->groupBy('nama_module')->get();
		$route = array();

		foreach ($routes as $val) {
			$route['']	 = '-- Silahkan Pilih Route --';
			$route[$val->nama_module] = $val->nama_module;
		}

		$users = User::all();
		$user = array();

		foreach ($users as $val) {
			$user['']   = '-- Silahkan Pilih User --';
			$user[$val->id] = $val->nama; 
		}
		
		$view->route = $route;
		$view->user  = $user;
		$view->search = 'yes';

		$this->layout->content = $view;
	}

}