<?php
/**
 * Laravel Date Class
 * Converting Any Date Format 
 *
 * @package	    Laravel
 * @subpackage	Libraries
 * @category	Libraries
 * @author	    Riky Fahri Hasibuan - riky.hasibuan@gmail.com
 */

class Date 
{
    public $year = "";
    public $month = "";
    public $day = "";
    public $date = "";
    public $input = '';

    public static function getYear($date) {
        $temp_date = explode("-", $date);
        $this->year = $temp_date[0];
        return $this->year;
    }

    public static function getMonth($date) {
        $temp_date = explode("-", $date);
        $this->month = $temp_date[1];
        return $this->month;
    }

    public static function getDay($date) {
        $temp_date = explode("-", $date);
        $this->day = $temp_date[2];
        return $this->day;
    }

    public static function reformatDate($date) {
        $temp_date = explode("-", $date);
        $new_date = array();
        for ($i = 3; $i > 0; $i--)
            $new_date[] = $temp_date[$i - 1];
        $date = implode("-", $new_date);
        return $date;
    }

    public static function convertDateTime($datetime) {
        $datetime_temp = explode(" ", $datetime);
        $temp_date = explode("-", $datetime_temp[0]);
        $new_date = array();
        for ($i = 3; $i > 0; $i--)
            $new_date[] = $temp_date[$i - 1];
        $date = implode("-", $new_date);
        return $datetime_temp[1]." ".$date;
    }

    public static function reformatTimeStamp($date) {
        $temp_date = explode("-", $date);
        $new_date = array();
        for ($i = 3; $i > 0; $i--)
            $new_date[] = $temp_date[$i - 1];
        $this->date = implode("-", $new_date);
        return $this->date;
    }

    public static function reformatDateSlash($date) {
        $temp_date = explode("-", $date);
        $new_date = array();
        for ($i = 3; $i > 0; $i--)
            $new_date[] = $temp_date[$i - 1];
        $this->date = implode("/", $new_date);
        return $this->date;
    }
    
    public static function reformatDateTimeSlash($date) {
    	$date = explode(" ", $date);
    	$temp_date = explode("-", $date[0]);
    	$new_date = array();
    	for ($i = 3; $i > 0; $i--)
    		$new_date[] = $temp_date[$i - 1];
    		$this->date = implode("/", $new_date);
        return $this->date;
    }

    public static function getIndoBulan($bln) {
        switch ($bln) {
            case 1: $month = "Januari"; break;
            case 2: $month = "Februari"; break;
            case 3: $month = "Maret"; break;
            case 4: $month = "April"; break;
            case 5: $month = "Mei"; break;
            case 6: $month = "Juni"; break;
            case 7: $month = "Juli"; break;
            case 8: $month = "Agustus"; break;
            case 9: $month = "September"; break;
            case 10: $month = "Oktober"; break;
            case 11: $month = "November"; break;
            case 12: $month = "Desember"; break;
        }
        return $month;
    }

    public function getIndoHari($date) {
        $temp_date = explode("-", $date);
        $day = date_format(date_create($date), 'D');
        $hari = '';
        switch ($day) {
            case "Mon": $hari = "Senin"; break;
            case "Tue": $hari = "Selasa"; break;
            case "Wed": $hari = "Rabu"; break;
            case "Thu": $hari = "Kamis"; break;
            case "Fri": $hari = "Jumat"; break;
            case "Sat": $hari = "Sabtu"; break;
            case "Sun": $hari = "Minggu"; break;
        }

        $bulan   = $this->getIndoBulan($temp_date[1]);
        $tahun   = $temp_date[0];
        $tanggal = $temp_date[2];
        $new_date = $hari." ".$tanggal.", ".$bulan." ".$tahun;
        return $new_date;
    } 

    public static function getIndoHariJam($date) {
        $date = explode(" ", $date);
        $temp_date = explode("-", $date[0]);
        $day = date_format(date_create($date[0]), 'D');
        $hari = '';
        switch ($day) {
            case "Mon": $hari = "Senin"; break;
            case "Tue": $hari = "Selasa"; break;
            case "Wed": $hari = "Rabu"; break;
            case "Thu": $hari = "Kamis"; break;
            case "Fri": $hari = "Jumat"; break;
            case "Sat": $hari = "Sabtu"; break;
            case "Sun": $hari = "Minggu"; break;
        }
        
        $bulan = Date::getIndoBulan($temp_date[1]);
        $tahun   = $temp_date[0];
        $tanggal = $temp_date[2];
        $new_date = date_format(date_create($date[1]), 'h:i:s A')." ".$tanggal."-".$temp_date[1]."-".$tahun;
        return $new_date;
    }
}